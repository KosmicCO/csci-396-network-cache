## Cache Server Setup

To setup a cache server, follow the given steps:

1. Start up the control server (see `control/CONTROL_SETUP.md`).
2. Set the perfered port to attach the web server to in `Rocket.toml` after `port =`. The default is 80.
3. Make sure that the ip and port of the control server are put into the file `crypto/control-ip.txt` with the ip on the first line and the port on the second line.
4. Make sure that the certificate `crypto/cert.pem` is the one associated with the control server (see `crypto/CRYPTO.md`).
5. Set the perfered redirect ip for this cache server in `cache/cache-ip.txt`, such as `http://myurl.com` or `8.8.8.8:50`.
6. Start `cache-server` in the root directory of the rust project.
