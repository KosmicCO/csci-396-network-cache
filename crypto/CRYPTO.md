## Generating Certificates

To generate a certificate, an RSA certificate must first be created and then converted to a PKCS12 certificate/key/password trio

### RSA Certificate and Key

To generate an RSA key, the following command can be used:
`openssl req -newkey rsa:2048 -new -x509 -nodes -keyout key.pem -out cert.pem -subj '/CN=<control-hostname>'`
which emits files `key.pem` and `cert.pem`. Make sure to use the same hostname from `<control-hostname>` for `crypto/control-ip.txt`.

### PKCS12 Certificate and Key

To convert to PKCS12, the following command can be used after generating the RSA certificate:
`openssl pkcs12 -export -out identity.pfx -inkey key.pem -in cert.pem`
Note that it will ask for a passward, which should be put into a file called `pass.txt` so that the program can decrypt the key.
