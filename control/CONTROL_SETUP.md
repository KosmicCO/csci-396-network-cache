## Control Server Setup

To setup the control server, follow the given steps:

1. Make sure that the domain name that the cache and dns servers will use to connect to the control server is a domain name that is registered with a web DNS server, since TLS requires that this be the case for security. Put the certificate password into `crypto/pass.txt` and the ip and port into `crypto/control-ip.txt`; ip on the first line and port on the second.
2. Make sure to generate the certicate (`cert.pem`) and identity (`identity.pfx`) using the instructions in `crypto/CYRPTO.md`. 
3. Start `control-server` in the root directory of the rust project.
