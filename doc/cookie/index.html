<!DOCTYPE html><html lang="en"><head><meta charset="utf-8"><meta name="viewport" content="width=device-width, initial-scale=1.0"><meta name="generator" content="rustdoc"><meta name="description" content="HTTP cookie parsing and cookie jar management."><meta name="keywords" content="rust, rustlang, rust-lang, cookie"><title>cookie - Rust</title><link rel="stylesheet" type="text/css" href="../normalize.css"><link rel="stylesheet" type="text/css" href="../rustdoc.css" id="mainThemeStyle"><link rel="stylesheet" type="text/css" href="../light.css"  id="themeStyle"><link rel="stylesheet" type="text/css" href="../dark.css" disabled ><link rel="stylesheet" type="text/css" href="../ayu.css" disabled ><script id="default-settings"></script><script src="../storage.js"></script><script src="../crates.js"></script><noscript><link rel="stylesheet" href="../noscript.css"></noscript><link rel="icon" type="image/svg+xml" href="../favicon.svg">
<link rel="alternate icon" type="image/png" href="../favicon-16x16.png">
<link rel="alternate icon" type="image/png" href="../favicon-32x32.png"><style type="text/css">#crate-search{background-image:url("../down-arrow.svg");}</style></head><body class="rustdoc mod"><!--[if lte IE 8]><div class="warning">This old browser is unsupported and will most likely display funky things.</div><![endif]--><nav class="sidebar"><div class="sidebar-menu" role="button">&#9776;</div><a href='../cookie/index.html'><div class='logo-container rust-logo'><img src='../rust-logo.png' alt='logo'></div></a><p class="location">Crate cookie</p><div class="block version"><p>Version 0.11.4</p></div><div class="sidebar-elems"><a id="all-types" href="all.html"><p>See all cookie's items</p></a><div class="block items"><ul><li><a href="#structs">Structs</a></li><li><a href="#enums">Enums</a></li></ul></div><p class="location"></p><div id="sidebar-vars" data-name="cookie" data-ty="mod" data-relpath="../"></div></div></nav><div class="theme-picker"><button id="theme-picker" aria-label="Pick another theme!" aria-haspopup="menu"><img src="../brush.svg" width="18" height="18" alt="Pick another theme!"></button><div id="theme-choices" role="menu"></div></div><script src="../theme.js"></script><nav class="sub"><form class="search-form"><div class="search-container"><div><select id="crate-search"><option value="All crates">All crates</option></select><input class="search-input" name="search" disabled autocomplete="off" spellcheck="false" placeholder="Click or press ‘S’ to search, ‘?’ for more options…" type="search"></div><button type="button" class="help-button">?</button>
                <a id="settings-menu" href="../settings.html"><img src="../wheel.svg" width="18" height="18" alt="Change settings"></a></div></form></nav><section id="main" class="content"><h1 class="fqn"><span class="in-band">Crate <a class="mod" href="">cookie</a></span><span class="out-of-band"><span id="render-detail"><a id="toggle-all-docs" href="javascript:void(0)" title="collapse all docs">[<span class="inner">&#x2212;</span>]</a></span><a class="srclink" href="../src/cookie/lib.rs.html#1-1136" title="goto source code">[src]</a></span></h1><div class="docblock"><p>HTTP cookie parsing and cookie jar management.</p>
<p>This crates provides the <a href="../cookie/struct.Cookie.html" title="Cookie"><code>Cookie</code></a> type, representing an HTTP cookie, and
the <a href="../cookie/struct.CookieJar.html" title="CookieJar"><code>CookieJar</code></a> type, which manages a collection of cookies for session
management, recording changes as they are made, and optional automatic
cookie encryption and signing.</p>
<h1 id="usage" class="section-header"><a href="#usage">Usage</a></h1>
<p>Add the following to the <code>[dependencies]</code> section of your <code>Cargo.toml</code>:</p>
<pre><code class="language-toml">cookie = &quot;0.11&quot;
</code></pre>
<p>Then add the following line to your crate root:</p>

<div class="example-wrap"><pre class="rust rust-example-rendered">
<span class="kw">extern</span> <span class="kw">crate</span> <span class="ident">cookie</span>;</pre></div>
<h1 id="features" class="section-header"><a href="#features">Features</a></h1>
<p>This crate exposes several features, all of which are disabled by default:</p>
<ul>
<li>
<p><strong><code>percent-encode</code></strong></p>
<p>Enables <em>percent encoding and decoding</em> of names and values in cookies.</p>
<p>When this feature is enabled, the <a href="../cookie/struct.Cookie.html#method.encoded" title="Cookie::encoded()"><code>Cookie::encoded()</code></a> and
<a href="../cookie/struct.Cookie.html#method.parse_encoded" title="Cookie::parse_encoded()"><code>Cookie::parse_encoded()</code></a> methods are available. The <code>encoded</code> method
returns a wrapper around a <code>Cookie</code> whose <code>Display</code> implementation
percent-encodes the name and value of the cookie. The <code>parse_encoded</code>
method percent-decodes the name and value of a <code>Cookie</code> during parsing.</p>
</li>
<li>
<p><strong><code>signed</code></strong></p>
<p>Enables <em>signed</em> cookies via <a href="../cookie/struct.CookieJar.html#method.signed" title="CookieJar::signed()"><code>CookieJar::signed()</code></a>.</p>
<p>When this feature is enabled, the <a href="../cookie/struct.CookieJar.html#method.signed" title="CookieJar::signed()"><code>CookieJar::signed()</code></a> method,
<a href="../cookie/struct.SignedJar.html" title="SignedJar"><code>SignedJar</code></a> type, and <a href="../cookie/struct.Key.html" title="Key"><code>Key</code></a> type are available. The jar acts as “child
jar”; operations on the jar automatically sign and verify cookies as they
are added and retrieved from the parent jar.</p>
</li>
<li>
<p><strong><code>private</code></strong></p>
<p>Enables <em>private</em> (authenticated, encrypted) cookies via
<a href="../cookie/struct.CookieJar.html#method.private" title="CookieJar::private()"><code>CookieJar::private()</code></a>.</p>
<p>When this feature is enabled, the <a href="../cookie/struct.CookieJar.html#method.private" title="CookieJar::private()"><code>CookieJar::private()</code></a> method,
<a href="../cookie/struct.PrivateJar.html" title="PrivateJar"><code>PrivateJar</code></a> type, and <a href="../cookie/struct.Key.html" title="Key"><code>Key</code></a> type are available. The jar acts as “child
jar”; operations on the jar automatically encrypt and decrypt/authenticate
cookies as they are added and retrieved from the parent jar.</p>
</li>
<li>
<p><strong><code>secure</code></strong></p>
<p>A meta-feature that simultaneously enables <code>signed</code> and <code>private</code>.</p>
</li>
</ul>
<p>You can enable features via <code>Cargo.toml</code>:</p>
<pre><code class="language-toml">[dependencies.cookie]
features = [&quot;secure&quot;, &quot;percent-encode&quot;]
</code></pre>
</div><h2 id="structs" class="section-header"><a href="#structs">Structs</a></h2>
<table><tr class="module-item"><td><a class="struct" href="struct.Cookie.html" title="cookie::Cookie struct">Cookie</a></td><td class="docblock-short"><p>Representation of an HTTP cookie.</p>
</td></tr><tr class="module-item"><td><a class="struct" href="struct.CookieBuilder.html" title="cookie::CookieBuilder struct">CookieBuilder</a></td><td class="docblock-short"><p>Structure that follows the builder pattern for building <code>Cookie</code> structs.</p>
</td></tr><tr class="module-item"><td><a class="struct" href="struct.CookieJar.html" title="cookie::CookieJar struct">CookieJar</a></td><td class="docblock-short"><p>A collection of cookies that tracks its modifications.</p>
</td></tr><tr class="module-item"><td><a class="struct" href="struct.Delta.html" title="cookie::Delta struct">Delta</a></td><td class="docblock-short"><p>Iterator over the changes to a cookie jar.</p>
</td></tr><tr class="module-item"><td><a class="struct" href="struct.EncodedCookie.html" title="cookie::EncodedCookie struct">EncodedCookie</a></td><td class="docblock-short"><p>Wrapper around <code>Cookie</code> whose <code>Display</code> implementation percent-encodes the
cookie’s name and value.</p>
</td></tr><tr class="module-item"><td><a class="struct" href="struct.Iter.html" title="cookie::Iter struct">Iter</a></td><td class="docblock-short"><p>Iterator over all of the cookies in a jar.</p>
</td></tr><tr class="module-item"><td><a class="struct" href="struct.Key.html" title="cookie::Key struct">Key</a></td><td class="docblock-short"><span class="stab portability" title="This is supported on crate features `private` or `signed` only"><code>private</code> or <code>signed</code></span><p>A cryptographic master key for use with <code>Signed</code> and/or <code>Private</code> jars.</p>
</td></tr><tr class="module-item"><td><a class="struct" href="struct.PrivateJar.html" title="cookie::PrivateJar struct">PrivateJar</a></td><td class="docblock-short"><span class="stab portability" title="This is supported on crate feature `private` only"><code>private</code></span><p>A child cookie jar that provides authenticated encryption for its cookies.</p>
</td></tr><tr class="module-item"><td><a class="struct" href="struct.SignedJar.html" title="cookie::SignedJar struct">SignedJar</a></td><td class="docblock-short"><span class="stab portability" title="This is supported on crate feature `signed` only"><code>signed</code></span><p>A child cookie jar that authenticates its cookies.</p>
</td></tr></table><h2 id="enums" class="section-header"><a href="#enums">Enums</a></h2>
<table><tr class="module-item"><td><a class="enum" href="enum.ParseError.html" title="cookie::ParseError enum">ParseError</a></td><td class="docblock-short"><p>Enum corresponding to a parsing error.</p>
</td></tr><tr class="module-item"><td><a class="enum" href="enum.SameSite.html" title="cookie::SameSite enum">SameSite</a></td><td class="docblock-short"><p>The <code>SameSite</code> cookie attribute.</p>
</td></tr></table></section><section id="search" class="content hidden"></section><section class="footer"></section><div id="rustdoc-vars" data-root-path="../" data-current-crate="cookie" data-search-js="../search-index.js"></div>
    <script src="../main.js"></script></body></html>