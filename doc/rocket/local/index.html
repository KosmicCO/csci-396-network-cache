<!DOCTYPE html><html lang="en"><head><meta charset="utf-8"><meta name="viewport" content="width=device-width, initial-scale=1.0"><meta name="generator" content="rustdoc"><meta name="description" content="Structures for local dispatching of requests, primarily for testing."><meta name="keywords" content="rust, rustlang, rust-lang, local"><title>rocket::local - Rust</title><link rel="stylesheet" type="text/css" href="../../normalize.css"><link rel="stylesheet" type="text/css" href="../../rustdoc.css" id="mainThemeStyle"><link rel="stylesheet" type="text/css" href="../../light.css"  id="themeStyle"><link rel="stylesheet" type="text/css" href="../../dark.css" disabled ><link rel="stylesheet" type="text/css" href="../../ayu.css" disabled ><script id="default-settings"></script><script src="../../storage.js"></script><script src="../../crates.js"></script><noscript><link rel="stylesheet" href="../../noscript.css"></noscript><link rel="shortcut icon" href="https://rocket.rs/v0.4/images/favicon.ico"><style type="text/css">#crate-search{background-image:url("../../down-arrow.svg");}</style></head><body class="rustdoc mod"><!--[if lte IE 8]><div class="warning">This old browser is unsupported and will most likely display funky things.</div><![endif]--><nav class="sidebar"><div class="sidebar-menu" role="button">&#9776;</div><a href='../../rocket/index.html'><div class='logo-container'><img src='https://rocket.rs/v0.4/images/logo-boxed.png' alt='logo'></div></a><p class="location">Module local</p><div class="sidebar-elems"><div class="block items"><ul><li><a href="#structs">Structs</a></li></ul></div><p class="location"><a href="../index.html">rocket</a></p><div id="sidebar-vars" data-name="local" data-ty="mod" data-relpath="../"></div><script defer src="../sidebar-items.js"></script></div></nav><div class="theme-picker"><button id="theme-picker" aria-label="Pick another theme!" aria-haspopup="menu"><img src="../../brush.svg" width="18" height="18" alt="Pick another theme!"></button><div id="theme-choices" role="menu"></div></div><script src="../../theme.js"></script><nav class="sub"><form class="search-form"><div class="search-container"><div><select id="crate-search"><option value="All crates">All crates</option></select><input class="search-input" name="search" disabled autocomplete="off" spellcheck="false" placeholder="Click or press ‘S’ to search, ‘?’ for more options…" type="search"></div><button type="button" class="help-button">?</button>
                <a id="settings-menu" href="../../settings.html"><img src="../../wheel.svg" width="18" height="18" alt="Change settings"></a></div></form></nav><section id="main" class="content"><h1 class="fqn"><span class="in-band">Module <a href="../index.html">rocket</a>::<wbr><a class="mod" href="">local</a></span><span class="out-of-band"><span id="render-detail"><a id="toggle-all-docs" href="javascript:void(0)" title="collapse all docs">[<span class="inner">&#x2212;</span>]</a></span><a class="srclink" href="../../src/rocket/local/mod.rs.html#1-105" title="goto source code">[src]</a></span></h1><div class="docblock"><p>Structures for local dispatching of requests, primarily for testing.</p>
<p>This module allows for simple request dispatching against a local,
non-networked instance of Rocket. The primary use of this module is to unit
and integration test Rocket applications by crafting requests, dispatching
them, and verifying the response.</p>
<h1 id="usage" class="section-header"><a href="#usage">Usage</a></h1>
<p>This module contains a <a href="../../rocket/local/struct.Client.html"><code>Client</code></a> structure that is used to create
<a href="../../rocket/local/struct.LocalRequest.html"><code>LocalRequest</code></a> structures that can be dispatched against a given
<a href="../../rocket/struct.Rocket.html"><code>Rocket</code></a> instance. Usage is straightforward:</p>
<ol>
<li>
<p>Construct a <code>Rocket</code> instance that represents the application.</p>

<div class="example-wrap"><pre class="rust rust-example-rendered">
<span class="kw">let</span> <span class="ident">rocket</span> <span class="op">=</span> <span class="ident">rocket</span>::<span class="ident">ignite</span>();</pre></div>
</li>
<li>
<p>Construct a <code>Client</code> using the <code>Rocket</code> instance.</p>

<div class="example-wrap"><pre class="rust rust-example-rendered">
<span class="kw">let</span> <span class="ident">client</span> <span class="op">=</span> <span class="ident">Client</span>::<span class="ident">new</span>(<span class="ident">rocket</span>).<span class="ident">expect</span>(<span class="string">&quot;valid rocket instance&quot;</span>);</pre></div>
</li>
<li>
<p>Construct requests using the <code>Client</code> instance.</p>

<div class="example-wrap"><pre class="rust rust-example-rendered">
<span class="kw">let</span> <span class="ident">req</span> <span class="op">=</span> <span class="ident">client</span>.<span class="ident">get</span>(<span class="string">&quot;/&quot;</span>);</pre></div>
</li>
<li>
<p>Dispatch the request to retrieve the response.</p>

<div class="example-wrap"><pre class="rust rust-example-rendered">
<span class="kw">let</span> <span class="ident">response</span> <span class="op">=</span> <span class="ident">req</span>.<span class="ident">dispatch</span>();</pre></div>
</li>
</ol>
<p>All together and in idiomatic fashion, this might look like:</p>

<div class="example-wrap"><pre class="rust rust-example-rendered">
<span class="kw">use</span> <span class="ident">rocket</span>::<span class="ident">local</span>::<span class="ident">Client</span>;

<span class="kw">let</span> <span class="ident">client</span> <span class="op">=</span> <span class="ident">Client</span>::<span class="ident">new</span>(<span class="ident">rocket</span>::<span class="ident">ignite</span>()).<span class="ident">expect</span>(<span class="string">&quot;valid rocket&quot;</span>);
<span class="kw">let</span> <span class="ident">response</span> <span class="op">=</span> <span class="ident">client</span>.<span class="ident">post</span>(<span class="string">&quot;/&quot;</span>)
    .<span class="ident">body</span>(<span class="string">&quot;Hello, world!&quot;</span>)
    .<span class="ident">dispatch</span>();</pre></div>
<h1 id="unitintegration-testing" class="section-header"><a href="#unitintegration-testing">Unit/Integration Testing</a></h1>
<p>This module can be used to test a Rocket application by constructing
requests via <code>Client</code> and validating the resulting response. As an example,
consider the following complete “Hello, world!” application, with testing.</p>

<div class="example-wrap"><pre class="rust rust-example-rendered">
<span class="attribute">#![<span class="ident">feature</span>(<span class="ident">proc_macro_hygiene</span>, <span class="ident">decl_macro</span>)]</span>

<span class="attribute">#[<span class="ident">macro_use</span>]</span> <span class="kw">extern</span> <span class="kw">crate</span> <span class="ident">rocket</span>;

<span class="attribute">#[<span class="ident">get</span>(<span class="string">&quot;/&quot;</span>)]</span>
<span class="kw">fn</span> <span class="ident">hello</span>() <span class="op">-</span><span class="op">&gt;</span> <span class="kw-2">&amp;</span><span class="lifetime">&#39;static</span> <span class="ident">str</span> {
    <span class="string">&quot;Hello, world!&quot;</span>
}

<span class="attribute">#[<span class="ident">cfg</span>(<span class="ident">test</span>)]</span>
<span class="kw">mod</span> <span class="ident">test</span> {
    <span class="kw">use</span> <span class="kw">super</span>::{<span class="ident">rocket</span>, <span class="ident">hello</span>};
    <span class="kw">use</span> <span class="ident">rocket</span>::<span class="ident">local</span>::<span class="ident">Client</span>;

    <span class="attribute">#[<span class="ident">test</span>]</span>
    <span class="kw">fn</span> <span class="ident">test_hello_world</span>() {
        <span class="comment">// Construct a client to use for dispatching requests.</span>
        <span class="kw">let</span> <span class="ident">rocket</span> <span class="op">=</span> <span class="ident">rocket</span>::<span class="ident">ignite</span>().<span class="ident">mount</span>(<span class="string">&quot;/&quot;</span>, <span class="macro">routes</span><span class="macro">!</span>[<span class="ident">hello</span>]);
        <span class="kw">let</span> <span class="ident">client</span> <span class="op">=</span> <span class="ident">Client</span>::<span class="ident">new</span>(<span class="ident">rocket</span>).<span class="ident">expect</span>(<span class="string">&quot;valid rocket instance&quot;</span>);

        <span class="comment">// Dispatch a request to &#39;GET /&#39; and validate the response.</span>
        <span class="kw">let</span> <span class="kw-2">mut</span> <span class="ident">response</span> <span class="op">=</span> <span class="ident">client</span>.<span class="ident">get</span>(<span class="string">&quot;/&quot;</span>).<span class="ident">dispatch</span>();
        <span class="macro">assert_eq</span><span class="macro">!</span>(<span class="ident">response</span>.<span class="ident">body_string</span>(), <span class="prelude-val">Some</span>(<span class="string">&quot;Hello, world!&quot;</span>.<span class="ident">into</span>()));
    }
}</pre></div>
</div><h2 id="structs" class="section-header"><a href="#structs">Structs</a></h2>
<table><tr class="module-item"><td><a class="struct" href="struct.Client.html" title="rocket::local::Client struct">Client</a></td><td class="docblock-short"><p>A structure to construct requests for local dispatching.</p>
</td></tr><tr class="module-item"><td><a class="struct" href="struct.LocalRequest.html" title="rocket::local::LocalRequest struct">LocalRequest</a></td><td class="docblock-short"><p>A structure representing a local request as created by <a href="../../rocket/local/struct.Client.html"><code>Client</code></a>.</p>
</td></tr><tr class="module-item"><td><a class="struct" href="struct.LocalResponse.html" title="rocket::local::LocalResponse struct">LocalResponse</a></td><td class="docblock-short"><p>A structure representing a response from dispatching a local request.</p>
</td></tr></table></section><section id="search" class="content hidden"></section><section class="footer"></section><div id="rustdoc-vars" data-root-path="../../" data-current-crate="rocket" data-search-js="../../search-index.js"></div>
    <script src="../../main.js"></script></body></html>