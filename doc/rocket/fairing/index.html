<!DOCTYPE html><html lang="en"><head><meta charset="utf-8"><meta name="viewport" content="width=device-width, initial-scale=1.0"><meta name="generator" content="rustdoc"><meta name="description" content="Fairings: callbacks at attach, launch, request, and response time."><meta name="keywords" content="rust, rustlang, rust-lang, fairing"><title>rocket::fairing - Rust</title><link rel="stylesheet" type="text/css" href="../../normalize.css"><link rel="stylesheet" type="text/css" href="../../rustdoc.css" id="mainThemeStyle"><link rel="stylesheet" type="text/css" href="../../light.css"  id="themeStyle"><link rel="stylesheet" type="text/css" href="../../dark.css" disabled ><link rel="stylesheet" type="text/css" href="../../ayu.css" disabled ><script id="default-settings"></script><script src="../../storage.js"></script><script src="../../crates.js"></script><noscript><link rel="stylesheet" href="../../noscript.css"></noscript><link rel="shortcut icon" href="https://rocket.rs/v0.4/images/favicon.ico"><style type="text/css">#crate-search{background-image:url("../../down-arrow.svg");}</style></head><body class="rustdoc mod"><!--[if lte IE 8]><div class="warning">This old browser is unsupported and will most likely display funky things.</div><![endif]--><nav class="sidebar"><div class="sidebar-menu" role="button">&#9776;</div><a href='../../rocket/index.html'><div class='logo-container'><img src='https://rocket.rs/v0.4/images/logo-boxed.png' alt='logo'></div></a><p class="location">Module fairing</p><div class="sidebar-elems"><div class="block items"><ul><li><a href="#structs">Structs</a></li><li><a href="#traits">Traits</a></li></ul></div><p class="location"><a href="../index.html">rocket</a></p><div id="sidebar-vars" data-name="fairing" data-ty="mod" data-relpath="../"></div><script defer src="../sidebar-items.js"></script></div></nav><div class="theme-picker"><button id="theme-picker" aria-label="Pick another theme!" aria-haspopup="menu"><img src="../../brush.svg" width="18" height="18" alt="Pick another theme!"></button><div id="theme-choices" role="menu"></div></div><script src="../../theme.js"></script><nav class="sub"><form class="search-form"><div class="search-container"><div><select id="crate-search"><option value="All crates">All crates</option></select><input class="search-input" name="search" disabled autocomplete="off" spellcheck="false" placeholder="Click or press ‘S’ to search, ‘?’ for more options…" type="search"></div><button type="button" class="help-button">?</button>
                <a id="settings-menu" href="../../settings.html"><img src="../../wheel.svg" width="18" height="18" alt="Change settings"></a></div></form></nav><section id="main" class="content"><h1 class="fqn"><span class="in-band">Module <a href="../index.html">rocket</a>::<wbr><a class="mod" href="">fairing</a></span><span class="out-of-band"><span id="render-detail"><a id="toggle-all-docs" href="javascript:void(0)" title="collapse all docs">[<span class="inner">&#x2212;</span>]</a></span><a class="srclink" href="../../src/rocket/fairing/mod.rs.html#1-439" title="goto source code">[src]</a></span></h1><div class="docblock"><p>Fairings: callbacks at attach, launch, request, and response time.</p>
<p>Fairings allow for structured interposition at various points in the
application lifetime. Fairings can be seen as a restricted form of
“middleware”. A fairing is an arbitrary structure with methods representing
callbacks that Rocket will run at requested points in a program. You can use
fairings to rewrite or record information about requests and responses, or
to perform an action once a Rocket application has launched.</p>
<p>To learn more about writing a fairing, see the <a href="../../rocket/fairing/trait.Fairing.html"><code>Fairing</code></a> trait
documentation. You can also use <a href="../../rocket/fairing/struct.AdHoc.html" title="AdHoc"><code>AdHoc</code></a> to create a fairing on-the-fly
from a closure or function.</p>
<h2 id="attaching" class="section-header"><a href="#attaching">Attaching</a></h2>
<p>You must inform Rocket about fairings that you wish to be active by calling
<a href="../../rocket/struct.Rocket.html#method.attach" title="Rocket::attach()"><code>Rocket::attach()</code></a> method on the application’s <a href="../../rocket/struct.Rocket.html" title="Rocket"><code>Rocket</code></a> instance and
passing in the appropriate <a href="../../rocket/fairing/trait.Fairing.html"><code>Fairing</code></a>. For instance, to attach fairings
named <code>req_fairing</code> and <code>res_fairing</code> to a new Rocket instance, you might
write:</p>

<div class="example-wrap"><pre class="rust rust-example-rendered">
<span class="kw">let</span> <span class="ident">rocket</span> <span class="op">=</span> <span class="ident">rocket</span>::<span class="ident">ignite</span>()
    .<span class="ident">attach</span>(<span class="ident">req_fairing</span>)
    .<span class="ident">attach</span>(<span class="ident">res_fairing</span>);</pre></div>
<p>Once a fairing is attached, Rocket will execute it at the appropriate time,
which varies depending on the fairing implementation. See the <a href="../../rocket/fairing/trait.Fairing.html"><code>Fairing</code></a>
trait documentation for more information on the dispatching of fairing
methods.</p>
<h2 id="ordering" class="section-header"><a href="#ordering">Ordering</a></h2>
<p><code>Fairing</code>s are executed in the order in which they are attached: the first
attached fairing has its callbacks executed before all others. Because
fairing callbacks may not be commutative, the order in which fairings are
attached may be significant. Because of this, it is important to communicate
to the user every consequence of a fairing.</p>
<p>Furthermore, a <code>Fairing</code> should take care to act locally so that the actions
of other <code>Fairings</code> are not jeopardized. For instance, unless it is made
abundantly clear, a fairing should not rewrite every request.</p>
</div><h2 id="structs" class="section-header"><a href="#structs">Structs</a></h2>
<table><tr class="module-item"><td><a class="struct" href="struct.AdHoc.html" title="rocket::fairing::AdHoc struct">AdHoc</a></td><td class="docblock-short"><p>A ad-hoc fairing that can be created from a function or closure.</p>
</td></tr><tr class="module-item"><td><a class="struct" href="struct.Info.html" title="rocket::fairing::Info struct">Info</a></td><td class="docblock-short"><p>Information about a <a href="../../rocket/fairing/trait.Fairing.html"><code>Fairing</code></a>.</p>
</td></tr><tr class="module-item"><td><a class="struct" href="struct.Kind.html" title="rocket::fairing::Kind struct">Kind</a></td><td class="docblock-short"><p>A bitset representing the kinds of callbacks a
<a href="../../rocket/fairing/trait.Fairing.html"><code>Fairing</code></a> wishes to receive.</p>
</td></tr></table><h2 id="traits" class="section-header"><a href="#traits">Traits</a></h2>
<table><tr class="module-item"><td><a class="trait" href="trait.Fairing.html" title="rocket::fairing::Fairing trait">Fairing</a></td><td class="docblock-short"><p>Trait implemented by fairings: Rocket’s structured middleware.</p>
</td></tr></table></section><section id="search" class="content hidden"></section><section class="footer"></section><div id="rustdoc-vars" data-root-path="../../" data-current-crate="rocket" data-search-js="../../search-index.js"></div>
    <script src="../../main.js"></script></body></html>