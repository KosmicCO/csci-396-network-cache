<!DOCTYPE html><html lang="en"><head><meta charset="utf-8"><meta name="viewport" content="width=device-width, initial-scale=1.0"><meta name="generator" content="rustdoc"><meta name="description" content="Trait implemented by types that can handle requests."><meta name="keywords" content="rust, rustlang, rust-lang, Handler"><title>rocket::Handler - Rust</title><link rel="stylesheet" type="text/css" href="../normalize.css"><link rel="stylesheet" type="text/css" href="../rustdoc.css" id="mainThemeStyle"><link rel="stylesheet" type="text/css" href="../light.css"  id="themeStyle"><link rel="stylesheet" type="text/css" href="../dark.css" disabled ><link rel="stylesheet" type="text/css" href="../ayu.css" disabled ><script id="default-settings"></script><script src="../storage.js"></script><script src="../crates.js"></script><noscript><link rel="stylesheet" href="../noscript.css"></noscript><link rel="shortcut icon" href="https://rocket.rs/v0.4/images/favicon.ico"><style type="text/css">#crate-search{background-image:url("../down-arrow.svg");}</style></head><body class="rustdoc trait"><!--[if lte IE 8]><div class="warning">This old browser is unsupported and will most likely display funky things.</div><![endif]--><nav class="sidebar"><div class="sidebar-menu" role="button">&#9776;</div><a href='../rocket/index.html'><div class='logo-container'><img src='https://rocket.rs/v0.4/images/logo-boxed.png' alt='logo'></div></a><p class="location">Trait Handler</p><div class="sidebar-elems"><div class="block items"><a class="sidebar-title" href="#required-methods">Required Methods</a><div class="sidebar-links"><a href="#tymethod.handle">handle</a></div><a class="sidebar-title" href="#implementors">Implementors</a></div><p class="location"><a href="index.html">rocket</a></p><div id="sidebar-vars" data-name="Handler" data-ty="trait" data-relpath=""></div><script defer src="sidebar-items.js"></script></div></nav><div class="theme-picker"><button id="theme-picker" aria-label="Pick another theme!" aria-haspopup="menu"><img src="../brush.svg" width="18" height="18" alt="Pick another theme!"></button><div id="theme-choices" role="menu"></div></div><script src="../theme.js"></script><nav class="sub"><form class="search-form"><div class="search-container"><div><select id="crate-search"><option value="All crates">All crates</option></select><input class="search-input" name="search" disabled autocomplete="off" spellcheck="false" placeholder="Click or press ‘S’ to search, ‘?’ for more options…" type="search"></div><button type="button" class="help-button">?</button>
                <a id="settings-menu" href="../settings.html"><img src="../wheel.svg" width="18" height="18" alt="Change settings"></a></div></form></nav><section id="main" class="content"><h1 class="fqn"><span class="in-band">Trait <a href="index.html">rocket</a>::<wbr><a class="trait" href="">Handler</a></span><span class="out-of-band"><span id="render-detail"><a id="toggle-all-docs" href="javascript:void(0)" title="collapse all docs">[<span class="inner">&#x2212;</span>]</a></span><a class="srclink" href="../src/rocket/handler.rs.html#134-146" title="goto source code">[src]</a></span></h1><div class="docblock type-decl hidden-by-usual-hider"><pre class="rust trait">pub trait Handler: <a class="trait" href="../rocket/handler/trait.Cloneable.html" title="trait rocket::handler::Cloneable">Cloneable</a> + <a class="trait" href="https://doc.rust-lang.org/nightly/core/marker/trait.Send.html" title="trait core::marker::Send">Send</a> + <a class="trait" href="https://doc.rust-lang.org/nightly/core/marker/trait.Sync.html" title="trait core::marker::Sync">Sync</a> + 'static {
    fn <a href="#tymethod.handle" class="fnname">handle</a>&lt;'r&gt;(&amp;self, request: &amp;'r <a class="struct" href="../rocket/request/struct.Request.html" title="struct rocket::request::Request">Request</a>&lt;'_&gt;, data: <a class="struct" href="../rocket/data/struct.Data.html" title="struct rocket::data::Data">Data</a>) -&gt; <a class="type" href="../rocket/handler/type.Outcome.html" title="type rocket::handler::Outcome">Outcome</a>&lt;'r&gt;;
}</pre></div><div class="docblock"><p>Trait implemented by types that can handle requests.</p>
<p>In general, you will never need to implement <code>Handler</code> manually or be
concerned about the <code>Handler</code> trait; Rocket’s code generation handles
everything for you. You only need to learn about this trait if you want to
provide an external, library-based mechanism to handle requests where
request handling depends on input from the user. In other words, if you want
to write a plugin for Rocket that looks mostly like a static route but need
user provided state to make a request handling decision, you should consider
implementing a custom <code>Handler</code>.</p>
<h1 id="example" class="section-header"><a href="#example">Example</a></h1>
<p>Say you’d like to write a handler that changes its functionality based on an
enum value that the user provides:</p>

<div class="example-wrap"><pre class="rust rust-example-rendered">
<span class="attribute">#[<span class="ident">derive</span>(<span class="ident">Copy</span>, <span class="ident">Clone</span>)]</span>
<span class="kw">enum</span> <span class="ident">Kind</span> {
    <span class="ident">Simple</span>,
    <span class="ident">Intermediate</span>,
    <span class="ident">Complex</span>,
}</pre></div>
<p>Such a handler might be written and used as follows:</p>

<div class="example-wrap"><pre class="rust rust-example-rendered">
<span class="kw">use</span> <span class="ident">rocket</span>::{<span class="ident">Request</span>, <span class="ident">Data</span>, <span class="ident">Route</span>, <span class="ident">http</span>::<span class="ident">Method</span>};
<span class="kw">use</span> <span class="ident">rocket</span>::<span class="ident">handler</span>::{<span class="self">self</span>, <span class="ident">Handler</span>, <span class="ident">Outcome</span>};

<span class="attribute">#[<span class="ident">derive</span>(<span class="ident">Clone</span>)]</span>
<span class="kw">struct</span> <span class="ident">CustomHandler</span>(<span class="ident">Kind</span>);

<span class="kw">impl</span> <span class="ident">Handler</span> <span class="kw">for</span> <span class="ident">CustomHandler</span> {
    <span class="kw">fn</span> <span class="ident">handle</span><span class="op">&lt;</span><span class="lifetime">&#39;r</span><span class="op">&gt;</span>(<span class="kw-2">&amp;</span><span class="self">self</span>, <span class="ident">req</span>: <span class="kw-2">&amp;</span><span class="lifetime">&#39;r</span> <span class="ident">Request</span>, <span class="ident">data</span>: <span class="ident">Data</span>) <span class="op">-</span><span class="op">&gt;</span> <span class="ident">Outcome</span><span class="op">&lt;</span><span class="lifetime">&#39;r</span><span class="op">&gt;</span> {
        <span class="kw">match</span> <span class="self">self</span>.<span class="number">0</span> {
            <span class="ident">Kind</span>::<span class="ident">Simple</span> <span class="op">=</span><span class="op">&gt;</span> <span class="ident">Outcome</span>::<span class="ident">from</span>(<span class="ident">req</span>, <span class="string">&quot;simple&quot;</span>),
            <span class="ident">Kind</span>::<span class="ident">Intermediate</span> <span class="op">=</span><span class="op">&gt;</span> <span class="ident">Outcome</span>::<span class="ident">from</span>(<span class="ident">req</span>, <span class="string">&quot;intermediate&quot;</span>),
            <span class="ident">Kind</span>::<span class="ident">Complex</span> <span class="op">=</span><span class="op">&gt;</span> <span class="ident">Outcome</span>::<span class="ident">from</span>(<span class="ident">req</span>, <span class="string">&quot;complex&quot;</span>),
        }
    }
}

<span class="kw">impl</span> <span class="ident">Into</span><span class="op">&lt;</span><span class="ident">Vec</span><span class="op">&lt;</span><span class="ident">Route</span><span class="op">&gt;</span><span class="op">&gt;</span> <span class="kw">for</span> <span class="ident">CustomHandler</span> {
    <span class="kw">fn</span> <span class="ident">into</span>(<span class="self">self</span>) <span class="op">-</span><span class="op">&gt;</span> <span class="ident">Vec</span><span class="op">&lt;</span><span class="ident">Route</span><span class="op">&gt;</span> {
        <span class="macro">vec</span><span class="macro">!</span>[<span class="ident">Route</span>::<span class="ident">new</span>(<span class="ident">Method</span>::<span class="ident">Get</span>, <span class="string">&quot;/&quot;</span>, <span class="self">self</span>)]
    }
}

<span class="kw">fn</span> <span class="ident">main</span>() {
    <span class="ident">rocket</span>::<span class="ident">ignite</span>()
        .<span class="ident">mount</span>(<span class="string">&quot;/&quot;</span>, <span class="ident">CustomHandler</span>(<span class="ident">Kind</span>::<span class="ident">Simple</span>))
        .<span class="ident">launch</span>();
}</pre></div>
<p>Note the following:</p>
<ol>
<li><code>CustomHandler</code> implements <code>Clone</code>. This is required so that
<code>CustomHandler</code> implements <code>Cloneable</code> automatically. The <code>Cloneable</code>
trait serves no other purpose but to ensure that every <code>Handler</code> can be
cloned, allowing <code>Route</code>s to be cloned.</li>
<li><code>CustomHandler</code> implements <code>Into&lt;Vec&lt;Route&gt;&gt;</code>, allowing an instance to
be used directly as the second parameter to <code>rocket.mount()</code>.</li>
<li>Unlike static-function-based handlers, this custom handler can make use
of any internal state.</li>
</ol>
<h1 id="alternatives" class="section-header"><a href="#alternatives">Alternatives</a></h1>
<p>The previous example could have been implemented using a combination of
managed state and a static route, as follows:</p>

<div class="example-wrap"><pre class="rust rust-example-rendered">
<span class="kw">use</span> <span class="ident">rocket</span>::<span class="ident">State</span>;

<span class="attribute">#[<span class="ident">get</span>(<span class="string">&quot;/&quot;</span>)]</span>
<span class="kw">fn</span> <span class="ident">custom_handler</span>(<span class="ident">state</span>: <span class="ident">State</span><span class="op">&lt;</span><span class="ident">Kind</span><span class="op">&gt;</span>) <span class="op">-</span><span class="op">&gt;</span> <span class="kw-2">&amp;</span><span class="lifetime">&#39;static</span> <span class="ident">str</span> {
    <span class="kw">match</span> <span class="kw-2">*</span><span class="ident">state</span> {
        <span class="ident">Kind</span>::<span class="ident">Simple</span> <span class="op">=</span><span class="op">&gt;</span> <span class="string">&quot;simple&quot;</span>,
        <span class="ident">Kind</span>::<span class="ident">Intermediate</span> <span class="op">=</span><span class="op">&gt;</span> <span class="string">&quot;intermediate&quot;</span>,
        <span class="ident">Kind</span>::<span class="ident">Complex</span> <span class="op">=</span><span class="op">&gt;</span> <span class="string">&quot;complex&quot;</span>,
    }
}

<span class="kw">fn</span> <span class="ident">main</span>() {
    <span class="ident">rocket</span>::<span class="ident">ignite</span>()
        .<span class="ident">mount</span>(<span class="string">&quot;/&quot;</span>, <span class="macro">routes</span><span class="macro">!</span>[<span class="ident">custom_handler</span>])
        .<span class="ident">manage</span>(<span class="ident">Kind</span>::<span class="ident">Simple</span>)
        .<span class="ident">launch</span>();
}</pre></div>
<p>Pros:</p>
<ul>
<li>The handler is easier to implement since Rocket’s code generation
ensures type-safety at all levels.</li>
</ul>
<p>Cons:</p>
<ul>
<li>Only one <code>Kind</code> can be stored in managed state. As such, only one
variant of the custom handler can be used.</li>
<li>The user must remember to manually call <code>rocket.manage(state)</code>.</li>
</ul>
<p>Use this alternative when a single configuration is desired and your custom
handler is private to your application. For all other cases, a custom
<code>Handler</code> implementation is preferred.</p>
</div><h2 id="required-methods" class="small-section-header">Required methods<a href="#required-methods" class="anchor"></a></h2><div class="methods"><h3 id="tymethod.handle" class="method"><code>fn <a href="#tymethod.handle" class="fnname">handle</a>&lt;'r&gt;(&amp;self, request: &amp;'r <a class="struct" href="../rocket/request/struct.Request.html" title="struct rocket::request::Request">Request</a>&lt;'_&gt;, data: <a class="struct" href="../rocket/data/struct.Data.html" title="struct rocket::data::Data">Data</a>) -&gt; <a class="type" href="../rocket/handler/type.Outcome.html" title="type rocket::handler::Outcome">Outcome</a>&lt;'r&gt;</code><a class="srclink" href="../src/rocket/handler.rs.html#145" title="goto source code">[src]</a></h3><div class="docblock"><p>Called by Rocket when a <code>Request</code> with its associated <code>Data</code> should be
handled by this handler.</p>
<p>The variant of <code>Outcome</code> returned determines what Rocket does next. If
the return value is a <code>Success(Response)</code>, the wrapped <code>Response</code> is
used to respond to the client. If the return value is a
<code>Failure(Status)</code>, the error catcher for <code>Status</code> is invoked to generate
a response. Otherwise, if the return value is <code>Forward(Data)</code>, the next
matching route is attempted. If there are no other matching routes, the
<code>404</code> error catcher is invoked.</p>
</div></div><span class="loading-content">Loading content...</span><h2 id="implementors" class="small-section-header">Implementors<a href="#implementors" class="anchor"></a></h2><div class="item-list" id="implementors-list"><h3 id="impl-Handler" class="impl"><code class="in-band">impl&lt;F:&nbsp;<a class="trait" href="https://doc.rust-lang.org/nightly/core/clone/trait.Clone.html" title="trait core::clone::Clone">Clone</a> + <a class="trait" href="https://doc.rust-lang.org/nightly/core/marker/trait.Sync.html" title="trait core::marker::Sync">Sync</a> + <a class="trait" href="https://doc.rust-lang.org/nightly/core/marker/trait.Send.html" title="trait core::marker::Send">Send</a> + 'static&gt; <a class="trait" href="../rocket/trait.Handler.html" title="trait rocket::Handler">Handler</a> for F <span class="where fmt-newline">where<br>&nbsp;&nbsp;&nbsp;&nbsp;F: <a class="trait" href="https://doc.rust-lang.org/nightly/core/ops/function/trait.Fn.html" title="trait core::ops::function::Fn">Fn</a>(&amp;'r <a class="struct" href="../rocket/request/struct.Request.html" title="struct rocket::request::Request">Request</a>&lt;'_&gt;, <a class="struct" href="../rocket/data/struct.Data.html" title="struct rocket::data::Data">Data</a>) -&gt; <a class="type" href="../rocket/handler/type.Outcome.html" title="type rocket::handler::Outcome">Outcome</a>&lt;'r&gt;,&nbsp;</span></code><a href="#impl-Handler" class="anchor"></a><a class="srclink" href="../src/rocket/handler.rs.html#172-179" title="goto source code">[src]</a></h3><div class="impl-items"><h4 id="method.handle" class="method hidden"><code>fn <a href="#method.handle" class="fnname">handle</a>&lt;'r&gt;(&amp;self, req: &amp;'r <a class="struct" href="../rocket/request/struct.Request.html" title="struct rocket::request::Request">Request</a>&lt;'_&gt;, data: <a class="struct" href="../rocket/data/struct.Data.html" title="struct rocket::data::Data">Data</a>) -&gt; <a class="type" href="../rocket/handler/type.Outcome.html" title="type rocket::handler::Outcome">Outcome</a>&lt;'r&gt;</code><a class="srclink" href="../src/rocket/handler.rs.html#176-178" title="goto source code">[src]</a></h4></div></div><span class="loading-content">Loading content...</span><script type="text/javascript" src="../implementors/rocket/trait.Handler.js" async></script></section><section id="search" class="content hidden"></section><section class="footer"></section><div id="rustdoc-vars" data-root-path="../" data-current-crate="rocket" data-search-js="../search-index.js"></div>
    <script src="../main.js"></script></body></html>