<!DOCTYPE html><html lang="en"><head><meta charset="utf-8"><meta name="viewport" content="width=device-width, initial-scale=1.0"><meta name="generator" content="rustdoc"><meta name="description" content="Success, failure, and forward handling."><meta name="keywords" content="rust, rustlang, rust-lang, outcome"><title>rocket::outcome - Rust</title><link rel="stylesheet" type="text/css" href="../../normalize.css"><link rel="stylesheet" type="text/css" href="../../rustdoc.css" id="mainThemeStyle"><link rel="stylesheet" type="text/css" href="../../light.css"  id="themeStyle"><link rel="stylesheet" type="text/css" href="../../dark.css" disabled ><link rel="stylesheet" type="text/css" href="../../ayu.css" disabled ><script id="default-settings"></script><script src="../../storage.js"></script><script src="../../crates.js"></script><noscript><link rel="stylesheet" href="../../noscript.css"></noscript><link rel="shortcut icon" href="https://rocket.rs/v0.4/images/favicon.ico"><style type="text/css">#crate-search{background-image:url("../../down-arrow.svg");}</style></head><body class="rustdoc mod"><!--[if lte IE 8]><div class="warning">This old browser is unsupported and will most likely display funky things.</div><![endif]--><nav class="sidebar"><div class="sidebar-menu" role="button">&#9776;</div><a href='../../rocket/index.html'><div class='logo-container'><img src='https://rocket.rs/v0.4/images/logo-boxed.png' alt='logo'></div></a><p class="location">Module outcome</p><div class="sidebar-elems"><div class="block items"><ul><li><a href="#enums">Enums</a></li><li><a href="#traits">Traits</a></li></ul></div><p class="location"><a href="../index.html">rocket</a></p><div id="sidebar-vars" data-name="outcome" data-ty="mod" data-relpath="../"></div><script defer src="../sidebar-items.js"></script></div></nav><div class="theme-picker"><button id="theme-picker" aria-label="Pick another theme!" aria-haspopup="menu"><img src="../../brush.svg" width="18" height="18" alt="Pick another theme!"></button><div id="theme-choices" role="menu"></div></div><script src="../../theme.js"></script><nav class="sub"><form class="search-form"><div class="search-container"><div><select id="crate-search"><option value="All crates">All crates</option></select><input class="search-input" name="search" disabled autocomplete="off" spellcheck="false" placeholder="Click or press ‘S’ to search, ‘?’ for more options…" type="search"></div><button type="button" class="help-button">?</button>
                <a id="settings-menu" href="../../settings.html"><img src="../../wheel.svg" width="18" height="18" alt="Change settings"></a></div></form></nav><section id="main" class="content"><h1 class="fqn"><span class="in-band">Module <a href="../index.html">rocket</a>::<wbr><a class="mod" href="">outcome</a></span><span class="out-of-band"><span id="render-detail"><a id="toggle-all-docs" href="javascript:void(0)" title="collapse all docs">[<span class="inner">&#x2212;</span>]</a></span><a class="srclink" href="../../src/rocket/outcome.rs.html#1-641" title="goto source code">[src]</a></span></h1><div class="docblock"><p>Success, failure, and forward handling.</p>
<p>The <code>Outcome&lt;S, E, F&gt;</code> type is similar to the standard library’s <code>Result&lt;S, E&gt;</code> type. It is an enum with three variants, each containing a value:
<code>Success(S)</code>, which represents a successful outcome, <code>Failure(E)</code>, which
represents a failing outcome, and <code>Forward(F)</code>, which represents neither a
success or failure, but instead, indicates that processing could not be
handled and should instead be <em>forwarded</em> to whatever can handle the
processing next.</p>
<p>The <code>Outcome</code> type is the return type of many of the core Rocket traits,
including <a href="../../rocket/request/trait.FromRequest.html"><code>FromRequest</code></a>, <a href="../../rocket/data/trait.FromData.html"><code>FromData</code></a>
<a href="../../rocket/response/trait.Responder.html"><code>Responder</code></a>. It is also the return type of request handlers via the
<a href="../../rocket/response/struct.Response.html"><code>Response</code></a> type.</p>
<h1 id="success" class="section-header"><a href="#success">Success</a></h1>
<p>A successful <code>Outcome&lt;S, E, F&gt;</code>, <code>Success(S)</code>, is returned from functions
that complete successfully. The meaning of a <code>Success</code> outcome depends on
the context. For instance, the <code>Outcome</code> of the <code>from_data</code> method of the
<a href="../../rocket/data/trait.FromData.html"><code>FromData</code></a> trait will be matched against the type expected by the user.
For example, consider the following handler:</p>

<div class='information'><div class='tooltip ignore'>ⓘ</div></div><div class="example-wrap"><pre class="rust rust-example-rendered ignore">
<span class="attribute">#[<span class="ident">post</span>(<span class="string">&quot;/&quot;</span>, <span class="ident">data</span> <span class="op">=</span> <span class="string">&quot;&lt;my_val&gt;&quot;</span>)]</span>
<span class="kw">fn</span> <span class="ident">hello</span>(<span class="ident">my_val</span>: <span class="ident">S</span>) <span class="op">-</span><span class="op">&gt;</span> ... {  }</pre></div>
<p>The <a href="../../rocket/data/trait.FromData.html"><code>FromData</code></a> implementation for the type <code>S</code> returns an <code>Outcome</code> with a
<code>Success(S)</code>. If <code>from_data</code> returns a <code>Success</code>, the <code>Success</code> value will
be unwrapped and the value will be used as the value of <code>my_val</code>.</p>
<h1 id="failure" class="section-header"><a href="#failure">Failure</a></h1>
<p>A failure <code>Outcome&lt;S, E, F&gt;</code>, <code>Failure(E)</code>, is returned when a function
fails with some error and no processing can or should continue as a result.
The meaning of a failure depends on the context.</p>
<p>In Rocket, a <code>Failure</code> generally means that a request is taken out of normal
processing. The request is then given to the catcher corresponding to some
status code. Users can catch failures by requesting a type of <code>Result&lt;S, E&gt;</code>
or <code>Option&lt;S&gt;</code> in request handlers. For example, if a user’s handler looks
like:</p>

<div class='information'><div class='tooltip ignore'>ⓘ</div></div><div class="example-wrap"><pre class="rust rust-example-rendered ignore">
<span class="attribute">#[<span class="ident">post</span>(<span class="string">&quot;/&quot;</span>, <span class="ident">data</span> <span class="op">=</span> <span class="string">&quot;&lt;my_val&gt;&quot;</span>)]</span>
<span class="kw">fn</span> <span class="ident">hello</span>(<span class="ident">my_val</span>: <span class="prelude-ty">Result</span><span class="op">&lt;</span><span class="ident">S</span>, <span class="ident">E</span><span class="op">&gt;</span>) <span class="op">-</span><span class="op">&gt;</span> ... {  }</pre></div>
<p>The <a href="../../rocket/data/trait.FromData.html"><code>FromData</code></a> implementation for the type <code>S</code> returns an <code>Outcome</code> with a
<code>Success(S)</code> and <code>Failure(E)</code>. If <code>from_data</code> returns a <code>Failure</code>, the
<code>Failure</code> value will be unwrapped and the value will be used as the <code>Err</code>
value of <code>my_val</code> while a <code>Success</code> will be unwrapped and used the <code>Ok</code>
value.</p>
<h1 id="forward" class="section-header"><a href="#forward">Forward</a></h1>
<p>A forward <code>Outcome&lt;S, E, F&gt;</code>, <code>Forward(F)</code>, is returned when a function
wants to indicate that the requested processing should be <em>forwarded</em> to the
next available processor. Again, the exact meaning depends on the context.</p>
<p>In Rocket, a <code>Forward</code> generally means that a request is forwarded to the
next available request handler. For example, consider the following request
handler:</p>

<div class='information'><div class='tooltip ignore'>ⓘ</div></div><div class="example-wrap"><pre class="rust rust-example-rendered ignore">
<span class="attribute">#[<span class="ident">post</span>(<span class="string">&quot;/&quot;</span>, <span class="ident">data</span> <span class="op">=</span> <span class="string">&quot;&lt;my_val&gt;&quot;</span>)]</span>
<span class="kw">fn</span> <span class="ident">hello</span>(<span class="ident">my_val</span>: <span class="ident">S</span>) <span class="op">-</span><span class="op">&gt;</span> ... {  }</pre></div>
<p>The <a href="../../rocket/data/trait.FromData.html"><code>FromData</code></a> implementation for the type <code>S</code> returns an <code>Outcome</code> with a
<code>Success(S)</code>, <code>Failure(E)</code>, and <code>Forward(F)</code>. If the <code>Outcome</code> is a
<code>Forward</code>, the <code>hello</code> handler isn’t called. Instead, the incoming request
is forwarded, or passed on to, the next matching route, if any. Ultimately,
if there are no non-forwarding routes, forwarded requests are handled by the
404 catcher. Similar to <code>Failure</code>s, users can catch <code>Forward</code>s by requesting
a type of <code>Option&lt;S&gt;</code>. If an <code>Outcome</code> is a <code>Forward</code>, the <code>Option</code> will be
<code>None</code>.</p>
</div><h2 id="enums" class="section-header"><a href="#enums">Enums</a></h2>
<table><tr class="module-item"><td><a class="enum" href="enum.Outcome.html" title="rocket::outcome::Outcome enum">Outcome</a></td><td class="docblock-short"><p>An enum representing success (<code>Success</code>), failure (<code>Failure</code>), or
forwarding (<code>Forward</code>).</p>
</td></tr></table><h2 id="traits" class="section-header"><a href="#traits">Traits</a></h2>
<table><tr class="module-item"><td><a class="trait" href="trait.IntoOutcome.html" title="rocket::outcome::IntoOutcome trait">IntoOutcome</a></td><td class="docblock-short"><p>Conversion trait from some type into an Outcome type.</p>
</td></tr></table></section><section id="search" class="content hidden"></section><section class="footer"></section><div id="rustdoc-vars" data-root-path="../../" data-current-crate="rocket" data-search-js="../../search-index.js"></div>
    <script src="../../main.js"></script></body></html>