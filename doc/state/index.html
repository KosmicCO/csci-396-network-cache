<!DOCTYPE html><html lang="en"><head><meta charset="utf-8"><meta name="viewport" content="width=device-width, initial-scale=1.0"><meta name="generator" content="rustdoc"><meta name="description" content="state - safe and effortless state management"><meta name="keywords" content="rust, rustlang, rust-lang, state"><title>state - Rust</title><link rel="stylesheet" type="text/css" href="../normalize.css"><link rel="stylesheet" type="text/css" href="../rustdoc.css" id="mainThemeStyle"><link rel="stylesheet" type="text/css" href="../light.css"  id="themeStyle"><link rel="stylesheet" type="text/css" href="../dark.css" disabled ><link rel="stylesheet" type="text/css" href="../ayu.css" disabled ><script id="default-settings"></script><script src="../storage.js"></script><script src="../crates.js"></script><noscript><link rel="stylesheet" href="../noscript.css"></noscript><link rel="icon" type="image/svg+xml" href="../favicon.svg">
<link rel="alternate icon" type="image/png" href="../favicon-16x16.png">
<link rel="alternate icon" type="image/png" href="../favicon-32x32.png"><style type="text/css">#crate-search{background-image:url("../down-arrow.svg");}</style></head><body class="rustdoc mod"><!--[if lte IE 8]><div class="warning">This old browser is unsupported and will most likely display funky things.</div><![endif]--><nav class="sidebar"><div class="sidebar-menu" role="button">&#9776;</div><a href='../state/index.html'><div class='logo-container rust-logo'><img src='../rust-logo.png' alt='logo'></div></a><p class="location">Crate state</p><div class="block version"><p>Version 0.4.2</p></div><div class="sidebar-elems"><a id="all-types" href="all.html"><p>See all state's items</p></a><div class="block items"><ul><li><a href="#structs">Structs</a></li></ul></div><p class="location"></p><div id="sidebar-vars" data-name="state" data-ty="mod" data-relpath="../"></div></div></nav><div class="theme-picker"><button id="theme-picker" aria-label="Pick another theme!" aria-haspopup="menu"><img src="../brush.svg" width="18" height="18" alt="Pick another theme!"></button><div id="theme-choices" role="menu"></div></div><script src="../theme.js"></script><nav class="sub"><form class="search-form"><div class="search-container"><div><select id="crate-search"><option value="All crates">All crates</option></select><input class="search-input" name="search" disabled autocomplete="off" spellcheck="false" placeholder="Click or press ‘S’ to search, ‘?’ for more options…" type="search"></div><button type="button" class="help-button">?</button>
                <a id="settings-menu" href="../settings.html"><img src="../wheel.svg" width="18" height="18" alt="Change settings"></a></div></form></nav><section id="main" class="content"><h1 class="fqn"><span class="in-band">Crate <a class="mod" href="">state</a></span><span class="out-of-band"><span id="render-detail"><a id="toggle-all-docs" href="javascript:void(0)" title="collapse all docs">[<span class="inner">&#x2212;</span>]</a></span><a class="srclink" href="../src/state/lib.rs.html#1-219" title="goto source code">[src]</a></span></h1><div class="docblock"><h1 id="state---safe-and-effortless-state-management" class="section-header"><a href="#state---safe-and-effortless-state-management">state - safe and effortless state management</a></h1>
<p>This crate allows you to safely and effortlessly manage global and/or
thread-local state. Three primitives are provided for state management:</p>
<ul>
<li><strong><a href="struct.Container.html">Container</a>:</strong> Type-based global and
thread-local storage for many values.</li>
<li><strong><a href="struct.Storage.html">Storage</a>:</strong> Global storage for a single instance.</li>
<li><strong><a href="struct.LocalStorage.html">LocalStorage</a>:</strong> Thread-local storage for a
single instance.</li>
</ul>
<h2 id="usage" class="section-header"><a href="#usage">Usage</a></h2>
<p>Include <code>state</code> in your <code>Cargo.toml</code> <code>[dependencies]</code>:</p>
<pre><code class="language-toml">[dependencies]
state = &quot;0.4&quot;
</code></pre>
<p>Thread-local state management is not enabled by default. You can enable it
via the <code>tls</code> feature:</p>
<pre><code class="language-toml">[dependencies]
state = { version = &quot;0.4&quot;, features = [&quot;tls&quot;] }
</code></pre>
<h2 id="use-cases" class="section-header"><a href="#use-cases">Use Cases</a></h2><h3 id="read-only-singleton" class="section-header"><a href="#read-only-singleton">Read-Only Singleton</a></h3>
<p>Suppose you have the following structure which is initialized in <code>main</code>
after receiving input from the user:</p>

<div class="example-wrap"><pre class="rust rust-example-rendered">
<span class="kw">struct</span> <span class="ident">Configuration</span> {
    <span class="ident">name</span>: <span class="ident">String</span>,
    <span class="ident">number</span>: <span class="ident">isize</span>,
    <span class="ident">verbose</span>: <span class="ident">bool</span>
}

<span class="kw">fn</span> <span class="ident">main</span>() {
    <span class="kw">let</span> <span class="ident">config</span> <span class="op">=</span> <span class="ident">Configuration</span> {
        <span class="comment">/* fill in structure at run-time from user input */</span>
    };
}</pre></div>
<p>You’d like to access this structure later, at any point in the program,
without any synchronization overhead. Prior to <code>state</code>, assuming you needed
to setup the structure after program start, your options were:</p>
<ol>
<li>Use <code>static mut</code> and <code>unsafe</code> to set an <code>Option&lt;Configuration&gt;</code> to
<code>Some</code>. Retrieve by checking for <code>Some</code>.</li>
<li>Use <code>lazy_static</code> with a <code>RwLock</code> to set an
<code>RwLock&lt;Option&lt;Configuration&gt;&gt;</code> to <code>Some</code>. Retrieve by <code>lock</code>ing and
checking for <code>Some</code>, paying the cost of synchronization.</li>
</ol>
<p>With <code>state</code>, you can use <a href="struct.LocalStorage.html">LocalStorage</a> and call
<code>set</code> and <code>get</code>, as follows:</p>

<div class="example-wrap"><pre class="rust rust-example-rendered">
<span class="kw">static</span> <span class="ident">CONFIG</span>: <span class="ident">LocalStorage</span><span class="op">&lt;</span><span class="ident">Configuration</span><span class="op">&gt;</span> <span class="op">=</span> <span class="ident">LocalStorage</span>::<span class="ident">new</span>();

<span class="kw">fn</span> <span class="ident">main</span>() {
    <span class="ident">CONFIG</span>.<span class="ident">set</span>(<span class="op">|</span><span class="op">|</span> <span class="ident">Configuration</span> {
        <span class="comment">/* fill in structure at run-time from user input */</span>
    });

    <span class="comment">/* at any point later in the program, in any thread */</span>
    <span class="kw">let</span> <span class="ident">config</span> <span class="op">=</span> <span class="ident">CONFIG</span>.<span class="ident">get</span>();
}</pre></div>
<h3 id="readwrite-singleton" class="section-header"><a href="#readwrite-singleton">Read/Write Singleton</a></h3>
<p>Following from the previous example, let’s now say that we want to be able
to modify our singleton <code>Configuration</code> structure as the program evolves. We
have two options:</p>
<ol>
<li>If we want to maintain the <em>same</em> state in any thread, we can use a
<code>Storage</code> structure and wrap our <code>Configuration</code> structure in a
synchronization primitive.</li>
<li>If we want to maintain <em>different</em> state in any thread, we can continue
to use a <code>LocalStorage</code> structure and wrap our <code>LocalStorage</code> type in a
<code>Cell</code> structure for internal mutability.</li>
</ol>
<p>In this example, we’ll choose <strong>1</strong>. The next example illustrates an
instance of <strong>2</strong>.</p>
<p>The following implements <strong>1</strong> by using a <code>Storage</code> structure and wrapping
the <code>Configuration</code> type with a <code>RwLock</code>:</p>

<div class="example-wrap"><pre class="rust rust-example-rendered">
<span class="kw">static</span> <span class="ident">CONFIG</span>: <span class="ident">Storage</span><span class="op">&lt;</span><span class="ident">RwLock</span><span class="op">&lt;</span><span class="ident">Configuration</span><span class="op">&gt;</span><span class="op">&gt;</span> <span class="op">=</span> <span class="ident">Storage</span>::<span class="ident">new</span>();

<span class="kw">fn</span> <span class="ident">main</span>() {
    <span class="kw">let</span> <span class="ident">config</span> <span class="op">=</span> <span class="ident">Configuration</span> {
        <span class="comment">/* fill in structure at run-time from user input */</span>
    };

    <span class="comment">// Make the config avaiable globally.</span>
    <span class="ident">CONFIG</span>.<span class="ident">set</span>(<span class="ident">RwLock</span>::<span class="ident">new</span>(<span class="ident">config</span>));

    <span class="comment">/* at any point later in the program, in any thread */</span>
    <span class="kw">let</span> <span class="ident">mut_config</span> <span class="op">=</span> <span class="ident">CONFIG</span>.<span class="ident">get</span>().<span class="ident">write</span>();
}</pre></div>
<h3 id="mutable-thread-local-data" class="section-header"><a href="#mutable-thread-local-data">Mutable, thread-local data</a></h3>
<p>Imagine you want to count the number of invocations to a function per
thread. You’d like to store the count in a <code>Cell&lt;usize&gt;</code> and use
<code>count.set(count.get() + 1)</code> to increment the count. Prior to <code>state</code>, your
only option was to use the <code>thread_local!</code> macro. <code>state</code> provides a more
flexible, and arguably simpler solution via <code>LocalStorage</code>. This scanario
is implemented in the folloiwng:</p>

<div class="example-wrap"><pre class="rust rust-example-rendered">
<span class="kw">static</span> <span class="ident">COUNT</span>: <span class="ident">LocalStorage</span><span class="op">&lt;</span><span class="ident">Cell</span><span class="op">&lt;</span><span class="ident">usize</span><span class="op">&gt;</span><span class="op">&gt;</span> <span class="op">=</span> <span class="ident">LocalStorage</span>::<span class="ident">new</span>();

<span class="kw">fn</span> <span class="ident">function_to_measure</span>() {
    <span class="kw">let</span> <span class="ident">count</span> <span class="op">=</span> <span class="ident">COUNT</span>.<span class="ident">get</span>();
    <span class="ident">count</span>.<span class="ident">set</span>(<span class="ident">count</span>.<span class="ident">get</span>() <span class="op">+</span> <span class="number">1</span>);
}

<span class="kw">fn</span> <span class="ident">main</span>() {
    <span class="comment">// setup the initializer for thread-local state</span>
    <span class="ident">COUNT</span>.<span class="ident">set</span>(<span class="op">|</span><span class="op">|</span> <span class="ident">Cell</span>::<span class="ident">new</span>(<span class="number">0</span>));

    <span class="comment">// spin up many threads that call `function_to_measure`.</span>
    <span class="kw">let</span> <span class="kw-2">mut</span> <span class="ident">threads</span> <span class="op">=</span> <span class="macro">vec</span><span class="macro">!</span>[];
    <span class="kw">for</span> <span class="ident">i</span> <span class="kw">in</span> <span class="number">0</span>..<span class="number">10</span> {
        <span class="ident">threads</span>.<span class="ident">push</span>(<span class="ident">thread</span>::<span class="ident">spawn</span>(<span class="op">|</span><span class="op">|</span> {
            <span class="comment">// Thread IDs may be reusued, so we reset the state.</span>
            <span class="ident">COUNT</span>.<span class="ident">get</span>().<span class="ident">set</span>(<span class="number">0</span>);
            <span class="ident">function_to_measure</span>();
            <span class="ident">COUNT</span>.<span class="ident">get</span>().<span class="ident">get</span>()
        }));
    }

    <span class="comment">// retrieve the total</span>
    <span class="kw">let</span> <span class="ident">total</span>: <span class="ident">usize</span> <span class="op">=</span> <span class="ident">threads</span>.<span class="ident">into_iter</span>()
        .<span class="ident">map</span>(<span class="op">|</span><span class="ident">t</span><span class="op">|</span> <span class="ident">t</span>.<span class="ident">join</span>().<span class="ident">unwrap</span>())
        .<span class="ident">sum</span>();

    <span class="macro">assert_eq</span><span class="macro">!</span>(<span class="ident">total</span>, <span class="number">10</span>);
}</pre></div>
<h2 id="performance" class="section-header"><a href="#performance">Performance</a></h2>
<p><code>state</code> is heavily tuned to perform optimally. <code>get{_local}</code> and
<code>set{_local}</code> calls to a <code>Container</code> incur overhead due to type lookup.
<code>Storage</code>, on the other hand, is optimal for global storage retrieval; it is
<em>slightly faster</em> than accessing global state initialized through
<code>lazy_static!</code>, more so across many threads. <code>LocalStorage</code> incurs slight
overhead due to thread lookup. However, <code>LocalStorage</code> has no
synchronization overhead, so retrieval from <code>LocalStorage</code> is faster than
through <code>Storage</code> across many threads.</p>
<p>Keep in mind that <code>state</code> allows global initialization at <em>any</em> point in the
program. Other solutions, such as <code>lazy_static!</code> and <code>thread_local!</code> allow
initialization <em>only</em> a priori. In other words, <code>state</code>’s abilities are a
superset of those provided by <code>lazy_static!</code> and <code>thread_local!</code>.</p>
<h2 id="when-to-use" class="section-header"><a href="#when-to-use">When To Use</a></h2>
<p>You should avoid using <code>state</code> as much as possible. Instead, thread state
manually throughout your program when feasible.</p>
</div><h2 id="structs" class="section-header"><a href="#structs">Structs</a></h2>
<table><tr class="module-item"><td><a class="struct" href="struct.Container.html" title="state::Container struct">Container</a></td><td class="docblock-short"><p>A container for global type-based state.</p>
</td></tr><tr class="module-item"><td><a class="struct" href="struct.Storage.html" title="state::Storage struct">Storage</a></td><td class="docblock-short"><p>A single storage location for global access to a value.</p>
</td></tr></table></section><section id="search" class="content hidden"></section><section class="footer"></section><div id="rustdoc-vars" data-root-path="../" data-current-crate="state" data-search-js="../search-index.js"></div>
    <script src="../main.js"></script></body></html>