use super::{dns_handler, cache_handler};
use crate::utils::*;
use super::updates_list::UpdateOp;
use super::{IDENTITY_PATH, PASSWORD_PATH};
use crate::ets;

use crate::log::*;
use concurrent_queue::ConcurrentQueue;
use native_tls::{Identity, TlsAcceptor, TlsStream};
use std::fs::{self, File};
use std::io::Read;
use std::net::{TcpListener, TcpStream};
use std::sync::atomic::AtomicBool;
use std::sync::atomic::Ordering;
use std::sync::Arc;
use std::thread;
use std::time::Duration;

const STANDBY_TIME: u64 = 200;
const CONN_WAIT_TIME: u64 = 2;

/// Possible tasks that the event loop will run.
pub enum Task {
    /// Stop processing tasks and save.
    Stop,
    /// Process a new dns server TLS connection.
    NewDNS(TlsStream<TcpStream>),
    /// Assign a new minor resource to a cache.
    NewResource((String, String)),
    /// Process a new cache server TLS connection.
    NewCache(TlsStream<TcpStream>),
    /// Assign a major to a cache server by ip.
    ManageMajor(String, String),
    /// Print all the registered cache servers.
    PrintRegistered,
}

/// Concurrent queue to function as the event queue.
pub struct Tasker(ConcurrentQueue<Task>);

impl Tasker {
    /// Creates a new instance.
    pub fn new() -> Self {
        Self(ConcurrentQueue::unbounded())
    }

    /// Pushes a new task onto the queue. Will only error if the queue was closed.
    pub fn push_task(&self, task: Task) -> Result<(), String> {
        self.0.push(task).map_err(ets)?;
        Ok(())
    }

    /// Gets the TLS identity from `crypto/identity.pfx`.
    fn get_tls_identity() -> Result<Identity, String> {
        let mut i_file = File::open(IDENTITY_PATH).map_err(ets)?;
        let mut identity = vec![];
        i_file.read_to_end(&mut identity).map_err(ets)?;
        let password = fs::read_to_string(PASSWORD_PATH).map_err(ets)?;
        let password = password.trim();
        debug!("Creating Identity");
        Identity::from_pkcs12(&identity, &password).map_err(ets)
    }

    /// Initializes the control server run loop.
    pub fn init_control(tasker: Arc<Tasker>, bind_ip: &str) -> Result<(), String> {
        let mut dns_handler = dns_handler::DNSHandle::load().map_err(ets)?;
        let mut cache_handler = cache_handler::CacheHandle::load().map_err(ets)?;

        let standby_time = Duration::from_millis(STANDBY_TIME);
        let running = Arc::new(AtomicBool::new(true));

        // Create thread for managing incoming connections.
        let new_conn_thread = {
            let running = running.clone();
            let tasker = tasker.clone();
            let conn_wait_time = Duration::from_millis(CONN_WAIT_TIME);
            let bind_ip = bind_ip.to_string();
            thread::spawn(move || -> Result<(), String> {
                info!("Getting connection information");
                let identity = Self::get_tls_identity()?;

                debug!("Binding TcpListener");
                let listener = TcpListener::bind(bind_ip).map_err(ets)?;
                listener.set_nonblocking(true).map_err(ets)?;
                debug!("Creating TlsAcceptor");
                let acceptor = TlsAcceptor::new(identity).map_err(ets)?;

                info!("Starting listener");
                while running.load(Ordering::Relaxed) {
                    match listener.accept() {
                        Ok((conn, addr)) => {
                            info!("Starting connection to {}", addr);
                            if conn.set_nonblocking(false).is_ok() {
                                match acceptor.accept(conn) {
                                    Ok(mut stream) => {
                                        if let Ok(is_dns) = de_recv(&mut stream) {
                                            if is_dns {
                                                tasker
                                                    .push_task(Task::NewDNS(stream))
                                                    .map_err(ets)?;
                                                info!("DNS connection created to {}.", addr);
                                            } else {
                                                tasker
                                                    .push_task(Task::NewCache(stream))
                                                    .map_err(ets)?;
                                                info!("Cache connection created to {}.", addr);
                                            }
                                        }
                                    }
                                    Err(e) => {
                                        warn!("Tls Connection error:\n{}", e);
                                    }
                                }
                            } else {
                                warn!("Failed to set TCP connection to be non-blocking.");
                            }
                        }
                        Err(ref e) if e.kind() == std::io::ErrorKind::WouldBlock => {
                            thread::sleep(conn_wait_time);
                        }
                        Err(e) => {
                            warn!("New connection aborted due to error:\n{}", e);
                        }
                    }
                }
                info!("Closing listener");
                Ok(())
            })
        };

        // Event loop.
        loop {
            let mut task = None;
            while task.is_none() {
                if tasker.0.len() == 0 {
                    std::thread::sleep(standby_time);
                }
                task = tasker.0.pop().ok();
            }

            match task.unwrap() {
                Task::Stop => {
                    break;
                }
                Task::NewDNS(stream) => {
                    if let Err(e) = dns_handler.handle_new_dns(stream) {
                        warn!("Error processing new dns connection:\n{}", e);
                    }
                }
                Task::NewResource(rsrc) => {
                    if let Some(ip) = cache_handler.handle_new_minor(rsrc.clone()) {
                        dns_handler.handle_update(rsrc, UpdateOp::Move(ip));
                    }
                }
                Task::NewCache(stream) => {
                    if let Err(e) = cache_handler.handle_new_cache(stream) {
                        warn!("Error processing new cache connection:\n{}", e);
                    }
                }
                Task::ManageMajor(maj, ip) => {
                    if let Err(e) = cache_handler.handle_major(maj.clone(), ip.clone()) {
                        warn!("Error managing major {} with {}:\n{}", maj, ip, e);
                    }
                }
                Task::PrintRegistered => {
                    info!("Currently registered connections:\n{:?}", cache_handler.get_registered()); 
                }
            }
        }

        // Stop processes.
        running.store(false, Ordering::SeqCst);
        let res = new_conn_thread.join().unwrap();

        dns_handler.close();
        cache_handler.close();
        tasker.0.close();
        res
    }
}
