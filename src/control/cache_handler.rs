use super::MANAGED_PATH;
use crate::cache::ManagedResources;
use crate::ets;
use crate::log::*;
use crate::utils::*;

use native_tls::TlsStream;
use serde::Deserialize as Des;
use serde::Serialize as Ser;
use std::cell::RefCell;
use std::collections::{HashMap, VecDeque};
use std::fs::File;
use std::net::TcpStream;
use std::path::PathBuf;
use std::rc::Rc;

/// Messages to be sent to cache servers.
#[derive(Ser, Des, Clone, Debug)]
pub enum CacheMessage {
    /// Close the connection.
    Close,
    /// Start to manage the given minor resource.
    Manage((String, String)),
    /// Start to manage the given major resource.
    ManageMajor(String),
}

/// Manages the cache resource distributions.
pub struct CacheHandle {
    rsrc_map: HashMap<String, RefCell<VecDeque<(Rc<RefCell<TlsStream<TcpStream>>>, String)>>>,
    conn_map: HashMap<String, Rc<RefCell<TlsStream<TcpStream>>>>,
    managed: ManagedResources,
    cur_dir: PathBuf,
}

impl CacheHandle {
    /// Loads the managed resources map from `control/managed.bin`
    pub fn load() -> Result<Self, String> {
        let cur_dir = PathBuf::from(env!("CARGO_MANIFEST_DIR"));

        let mut managed_path = cur_dir.clone();
        managed_path.push(MANAGED_PATH);

        let managed = if let Ok(file) = File::open(managed_path) {
            info!("Loading in managed map.");
            bincode::deserialize_from(file).map_err(ets)?
        } else {
            info!("Creating new managed map.");
            ManagedResources::new()
        };

        Ok(Self {
            rsrc_map: HashMap::new(),
            conn_map: HashMap::new(),
            cur_dir,
            managed,
        })
    }

    /// Saves the managed resources map from `control/managed.bin`.
    pub fn save(&self) -> Result<(), String> {
        let mut managed_path = self.cur_dir.clone();
        managed_path.push(MANAGED_PATH);
        bincode::serialize_into(File::create(managed_path).map_err(ets)?, &self.managed)
            .map_err(ets)?;
        Ok(())
    }

    /// Gets the necessary information from a new TLS connection to a cache server.
    pub fn handle_new_cache(&mut self, mut stream: TlsStream<TcpStream>) -> Result<(), String> {
        debug!("Getting managed majors and cache ip.");
        let ip: String = de_recv(&mut stream)?;
        let majors: Vec<String> = de_recv(&mut stream)?;

        let cell_stream = Rc::new(RefCell::new(stream));

        self.conn_map.insert(ip.clone(), cell_stream.clone()); // This overrides the previous connection!

        for s in majors {
            let in_map = self.rsrc_map.contains_key(&s);
            if !in_map {
                self.rsrc_map
                    .insert(s.clone(), RefCell::new(VecDeque::new()));
            }
            self.rsrc_map
                .get(&s)
                .unwrap()
                .borrow_mut()
                .push_back((cell_stream.clone(), ip.clone()));
        }

        debug!("Finished sending updates.");

        Ok(())
    }

    /// Gets a list of the ips associated with each cache connection.
    pub fn get_registered(&self) -> Vec<String> {
        self.conn_map.iter().map(|(k, _)| k.clone()).collect::<Vec<String>>()
    }

    /// Decides which cache server to assign the new minor to depending on a rotating queue and
    /// whether the given server manages the major that the minor is associated with. Note that
    /// this function will return `None` if there are no valid cache servers to assign the minor
    /// too.
    pub fn handle_new_minor(&mut self, rsrc: (String, String)) -> Option<String> {
        if self.managed.is_managed(&rsrc.0, &rsrc.1).ok()? {
            warn!("Resource already added.");
            return None;
        }

        let mut cache = None;
        let mut deque = self.rsrc_map.get(&rsrc.0)?.borrow_mut();
        // Goes through trying to find an open cache
        while cache.is_none() {
            if let Some((stream, ip)) = deque.pop_front() {
                if let Ok(_) = ser_send_cell(&CacheMessage::Manage(rsrc.clone()), &stream) {
                    cache = Some((stream, ip));
                }
            } else {
                break;
            }
        }

        // Requeues cache if it worked. Does the job of weeding out closed connections and
        // balancing resources between cache servers.
        if let Some((stream, ip)) = cache {
            debug!("Pushed resource to cache server at {}.", ip);
            self.managed.manage_minor(&rsrc.0, &rsrc.1).unwrap(); // Cant really do anything about the error here
            deque.push_back((stream, ip.clone()));
            return Some(ip);
        }
        None
    }

    /// Assigns the cache server with the given ip the major. will return an error if the cache
    /// server already manages the major.
    pub fn handle_major(&mut self, major: String, ip: String) -> Result<(), String> {
        let stream = self.conn_map.get(&ip).ok_or("Cache not registered.")?;
        ser_send_cell(&CacheMessage::ManageMajor(major.clone()), stream)?;
        let not_managed: bool = de_recv_cell(stream)?;
        not_managed
            .then_some(())
            .ok_or("Cache already managing major.")?;

        let in_map = self.rsrc_map.contains_key(&major);
        if !in_map {
            self.rsrc_map
                .insert(major.clone(), RefCell::new(VecDeque::new()));
        }
        self.rsrc_map
            .get(&major)
            .unwrap()
            .borrow_mut()
            .push_back((stream.clone(), ip.clone()));
        Ok(())
    }

    /// Closes all the cache connections and saves.
    pub fn close(self) {
        let _ = self.save();
        for (_, deque) in self.rsrc_map {
            for (stream, _) in deque.borrow().iter() {
                let _ = ser_send_cell(&CacheMessage::Close, &stream);
                let _ = stream.borrow_mut().shutdown();
            }
        }
    }
}
