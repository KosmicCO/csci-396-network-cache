
pub mod updates_list;

pub mod dns_handler;

pub mod tasker;

pub mod cache_handler;


const UPDATES_PATH: &str = "control/updates.bin";
const MANAGED_PATH: &str = "control/managed.bin";

const IDENTITY_PATH: &str = "crypto/identity.pfx";
const PASSWORD_PATH: &str = "crypto/pass.txt";
pub const CERTIFICATE_PATH: &str = "crypto/cert.pem";
