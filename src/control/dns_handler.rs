use super::updates_list::*;
use super::UPDATES_PATH;
use crate::utils::*;
use crate::ets;

use bincode;
use native_tls::TlsStream;
use std::net::TcpStream;
use std::cell::RefCell;
use std::collections::BTreeMap;
use std::fs::File;
use std::path::PathBuf;
use std::time::SystemTime;
use serde::Serialize as Ser;
use serde::Deserialize as Des;

use crate::log::*;

/// Handles updating the DNS servers of changes to the storage locations, etc.
pub struct DNSHandle {
    next_id: u64,
    conn_map: BTreeMap<u64, RefCell<TlsStream<TcpStream>>>,
    updates_list: UpdatesList,
    cur_dir: PathBuf,
}

/// A message to be sent over the network to a dns server.
#[derive(Ser, Des, Clone, Debug)]
pub enum DNSMessage {
    /// Close the connection.
    Close,
    /// Update the cache server managing the resource.
    Update((String, String), UpdateOp),
}

impl DNSHandle {
    /// Loads the `UpdatesList` from `control/updates.bin` to create an instance. Creates a new
    /// instance if the file does not exist.
    pub fn load() -> Result<Self, String> {
        let cur_dir = PathBuf::from(env!("CARGO_MANIFEST_DIR"));

        let mut updates_path = cur_dir.clone();
        updates_path.push(UPDATES_PATH);

        let updates_list = if let Ok(file) = File::open(updates_path) {
            info!("Loading in updates list.");
            bincode::deserialize_from(file).map_err(ets)?
        } else {
            info!("Creating new updates list");
            UpdatesList::new()
        };

        Ok(Self {
            next_id: 0,
            conn_map: BTreeMap::new(),
            updates_list,
            cur_dir,
        })
    }

    /// Saves the `UpdatesList` to `control/updates.bin`.
    pub fn save(&self) -> Result<(), String> {
        let mut updates_path = self.cur_dir.clone();
        updates_path.push(UPDATES_PATH);
        bincode::serialize_into(
            File::create(updates_path).map_err(ets)?,
            &self.updates_list,
        )
        .map_err(ets)?;
        Ok(())
    }

    /// Gets the necessary information from a new TLS connection to a dns server.
    pub fn handle_new_dns(&mut self, mut stream: TlsStream<TcpStream>) -> Result<(), String> {
        let id = self.next_id;
        self.next_id += 1;
        debug!("Getting last update time.");
        let last_update_time: Option<SystemTime> = de_recv(&mut stream)?;

        let bytes = bincode::serialize(&last_update_time).map_err(ets)?;
        debug!("{:?}", bytes);

        let cell_stream = RefCell::new(stream);
        debug!("Updating DNS from {:?}.", last_update_time);
        self.updates_list
            .send_updates(last_update_time, |rsrc, update| {
                debug!("Sending catchup update.");
                ser_send_cell(&DNSMessage::Update(rsrc, update), &cell_stream)?;
                Ok(())
            })?;

        self.conn_map.insert(id, cell_stream);

        debug!("Finished sending updates.");

        Ok(())
    }

    /// Applies the new update to the `UpdatesList` and sends it out to all the dns servers.
    pub fn handle_update(&mut self, rsrc: (String, String), update: UpdateOp) {
        self.updates_list.update(rsrc.clone(), update.clone());
        let mut to_close = Vec::new();
        for (id, dns) in &self.conn_map {
            // TODO: Print error messages
            if let Err(e) = ser_send_cell(&DNSMessage::Update(rsrc.clone(), update.clone()), &dns) {
                to_close.push(id.clone());
                warn!("Error sending updates:\n{}", e);
            }
        }

        // Close the broken conns
        for id in to_close {
            let _ = self.conn_map.remove(&id).unwrap().borrow_mut().shutdown();
        }
    }

    /// Closes all the dns connections and saves.
    pub fn close(self) {
        // TODO: Emit an error message of some sort
        let _ = self.save();
        for conn in self.conn_map {
            if ser_send_cell(&DNSMessage::Close, &conn.1).is_ok() {
                let _ = conn.1.borrow_mut().shutdown();
            }
        }
    }
}
