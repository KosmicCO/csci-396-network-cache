use lru::LruCache;
use std::time::SystemTime;
use serde::Serialize as Ser;
use serde::Deserialize as Des;
use serde::ser::{Serializer, Serialize};
use serde::de::{Deserializer, Deserialize};

/// Update operations on resources
#[derive(Ser, Des, Clone, Debug, PartialEq)]
pub enum UpdateOp {
    /// Resource was deleted.
    Delete,         
    /// Moved or created on specified cache server.
    Move(String),
}

/// Stores an ordered list of resources paired with updates and the time they were made so that a
/// new dns server can receive all the updates to the time it went offline.
pub struct UpdatesList(LruCache<(String, String), (UpdateOp, SystemTime)>);

impl UpdatesList {
    /// Creates a new instance.
    pub fn new() -> Self {
        Self(LruCache::unbounded())
    }

    /// Puts a new update into the list.
    pub fn update(&mut self, rsrc: (String, String), update: UpdateOp) {
        self.0.put(rsrc, (update, SystemTime::now()));
    }

    /// Sends the missed updates to the writer. The `write` variable is a closure that takes in an
    /// update and should use it in some manner (such as to write to a stream, etc.).
    pub fn send_updates<FW>(&self, last_updated: Option<SystemTime>, mut write: FW) -> Result<(), String>
    where
        FW: FnMut((String, String), UpdateOp) -> Result<(), String>,
    {
        for (rsrc, (update, time)) in self.0.iter() {
            if last_updated.clone().map(|lt| time > &lt).unwrap_or(false) {
                break;
            }
            write(rsrc.clone(), update.clone())?;
        }
        Ok(())
    }
}

/// Makes the `UpdatesList` serializable by converting it to a list of tuples.
impl Serialize for UpdatesList {
    fn serialize<S: Serializer>(&self, serializer: S) -> Result<S::Ok, S::Error> {
        let num = self.0.len();
        let mut ser_vec: Vec<((String, String), (UpdateOp, SystemTime))> = Vec::with_capacity(num);
        for (k, v) in self.0.iter().rev() {
            ser_vec.push((k.clone(), v.clone()));
        }
        ser_vec.serialize(serializer)
    }
}

/// Makes the `UpdatesList` deserializable by reading a list of tuples.
impl<'de> Deserialize<'de> for UpdatesList {
    fn deserialize<D: Deserializer<'de>>(deserializer: D) -> Result<Self, D::Error> {
        let de_vec: Vec<((String, String), (UpdateOp, SystemTime))> = Vec::deserialize(deserializer)?;
        let mut ret = UpdatesList::new();
        for (k, v) in de_vec.iter() {
            ret.0.put(k.clone(), v.clone());
        }
        Ok(ret)
    }
}

#[cfg(test)]
mod test {
    use super::UpdatesList;
    use super::UpdateOp::*;

    #[test]
    fn updates_list_serde() {
        let mut list = UpdatesList::new();
        let r1 = ("one".to_string(), "d-one".to_string());
        let r2 = ("one".to_string(), "d-two".to_string());
        let r3 = ("two".to_string(), "d-one".to_string());
        list.update(r1.clone(), Move("109.93.28.182".to_string()));
        list.update(r2.clone(), Move("94.12.82.183".to_string()));
        list.update(r3.clone(), Delete);
        let ser = serde_yaml::to_string(&list).unwrap();
        let de: UpdatesList = serde_yaml::from_str(&ser).unwrap();
        
        assert_eq!(list.0.len(), de.0.len());
        assert_eq!(list.0.peek(&r1).unwrap().1, de.0.peek(&r1).unwrap().1); 
        assert_eq!(list.0.peek(&r2).unwrap().1, de.0.peek(&r2).unwrap().1); 
        assert_eq!(list.0.peek(&r3).unwrap().1, de.0.peek(&r3).unwrap().1); 
    }
}
