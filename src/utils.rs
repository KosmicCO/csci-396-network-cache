use crate::ets;
use crate::CONTROL_IP_PATH;

use serde::ser::Serialize;
use serde::de::DeserializeOwned;
use std::io::{Read, Write};
use std::cell::RefCell;
use std::io::{self, BufRead};
use std::path::{Path, PathBuf};
use std::fs::File;
use bincode;

/// Creates a line iterator that iterates through the lines of a file.
pub fn read_lines<P>(filename: P) -> io::Result<io::Lines<io::BufReader<File>>>
where P: AsRef<Path>, {
    let file = File::open(filename)?;
    Ok(io::BufReader::new(file).lines())
}

/// Serializes and sends an object over the provided writer.
pub fn ser_send(obj: &impl Serialize, write: &mut impl Write) -> Result<(), String> {
    let mut bytes = bincode::serialize(obj).map_err(ets)?;
    let length: u64 = bytes.len() as u64;
    let mut send_bytes = bincode::serialize(&length).map_err(ets)?;
    send_bytes.append(&mut bytes);
    write.write_all(&send_bytes).map_err(ets)
}

/// Seralizes and sends an object over the provided writer. Does the exact same thing as
/// `ser_send`, except takes in a `RefCell` instead of the bare `Write` since they do not share
/// pointer traits.
pub fn ser_send_cell(obj: &impl Serialize, write: &RefCell<impl Write>) -> Result<(), String> {
    let mut bytes = bincode::serialize(obj).map_err(ets)?;
    let length: u64 = bytes.len() as u64;
    let mut send_bytes = bincode::serialize(&length).map_err(ets)?;
    send_bytes.append(&mut bytes);
    write.borrow_mut().write_all(&send_bytes).map_err(ets)
}

/// Deserializes an object from the provided reader.
pub fn de_recv<D: DeserializeOwned>(read: &mut impl Read) -> Result<D, String> {
    let mut length_bytes = [0u8; 8];
    read.read_exact(&mut length_bytes).map_err(ets)?;
    let length: u64 = bincode::deserialize(&length_bytes).map_err(ets)?;
    let mut bytes = vec![0u8; length as usize];
    read.read_exact(&mut bytes).map_err(ets)?;
    bincode::deserialize::<D>(&bytes).map_err(ets)
}

/// Deserializes an object from the provided reader. Does the exact same thing as
/// `ser_send`, except takes in a `RefCell` instead of the bare `Read` since they do not share
/// pointer traits.
pub fn de_recv_cell<D: DeserializeOwned>(read: &RefCell<impl Read>) -> Result<D, String> {
    let mut length_bytes = [0u8; 8];
    read.borrow_mut().read_exact(&mut length_bytes).map_err(ets)?;
    let length: u64 = bincode::deserialize(&length_bytes).map_err(ets)?;
    let mut bytes = vec![0u8; length as usize];
    read.borrow_mut().read_exact(&mut bytes).map_err(ets)?;
    bincode::deserialize::<D>(&bytes).map_err(ets)
}

/// Sets up a fern logger.
pub fn setup_logger(path: &str) -> Result<(), fern::InitError> {
    fern::Dispatch::new()
        .format(|out, message, record| {
            out.finish(format_args!(
                "\r{}[{}][{}] {}",
                chrono::Local::now().format("[%Y-%m-%d][%H:%M:%S]"),
                record.target(),
                record.level(),
                message
            ))
        })
        .level(log::LevelFilter::Debug)
        .chain(std::io::stdout())
        .chain(std::fs::OpenOptions::new().write(true).create(true).open(path)?)
        .apply()?;
    Ok(())
}

/// Gets the ip and port of the control server from `crypto/control-ip.txt`.
pub fn get_control_ip() -> (String, u16) {

    let mut ip_file = PathBuf::from(env!("CARGO_MANIFEST_DIR"));
    ip_file.push(CONTROL_IP_PATH);
    let mut control_ip_lines =
        read_lines(ip_file).expect("Failed to read file crypto/control-ip.txt");
    let ip = control_ip_lines
        .next()
        .expect("Failed to get the ip of the control server from crypto/control-ip.txt.")
        .expect("Failed to get the ip of the control server from crypto/control-ip.txt.");
    let port = control_ip_lines
        .next()
        .expect("Failed to parse port of the control server from crypto/control-ip.txt.")
        .expect("Failed to parse port of the control server from crypto/control-ip.txt.")
        .parse::<u16>()
        .expect("Invalid port from crypto/control-ip.txt");

    (ip, port)
}
