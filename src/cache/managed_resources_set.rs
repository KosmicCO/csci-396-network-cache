use crate::ets;

use serde::de::{Deserialize, Deserializer};
use serde::ser::{Error, Serialize, Serializer};
use std::collections::{HashMap, HashSet};
use std::iter::FromIterator;
use std::sync::{Arc, RwLock};

/// Map of major-minor resource pairs that the cache server manages.
pub struct ManagedResources(RwLock<HashMap<String, Arc<RwLock<HashSet<String>>>>>);

impl ManagedResources {
    /// Creates a new instance.
    pub fn new() -> Self {
        Self(RwLock::new(HashMap::new()))
    }

    /// Insert major key to manage.
    pub fn manage_major(&self, mk: &str) -> Result<(), String> {
        let has_major = { self.0.read().map_err(ets)?.contains_key(mk) };
        if !has_major {
            self.0
                .write()
                .map_err(ets)?
                .insert(mk.to_string(), Arc::new(RwLock::new(HashSet::new())));
        }
        Ok(())
    }

    /// Insert minor key to manage.
    pub fn manage_minor(&self, mk: &str, k: &str) -> Result<(), String> {
        let min_set = { self.0.read().map_err(ets)?.get(mk).map(Arc::clone) }; // Makes sure that the lock is dropped

        if let Some(min_set) = min_set {
            min_set.write().map_err(ets)?.insert(k.to_string());
            return Ok(());
        }

        let mut nhs = HashSet::new();
        nhs.insert(k.to_string());
        self.0
            .write()
            .map_err(ets)?
            .insert(mk.to_string(), Arc::new(RwLock::new(nhs)));
        Ok(())
    }

    /// Stops managing the given minor key. Does not stop managing the major key.
    pub fn unmanage_minor(&self, mk: &str, k: &str) -> Result<(), String> {
        let min_set = { self.0.read().map_err(ets)?.get(mk).map(Arc::clone) }; // Makes sure that the lock is dropped

        min_set
            .map(|x| -> Result<(), String> {
                x.write().map_err(ets)?.remove(k);
                Ok(())
            })
            .transpose()?;
        Ok(())
    }

    /// Stops managing the given major key.
    pub fn unmanage_major(&self, mk: &str) -> Result<(), String> {
        self.0.write().map_err(ets)?.remove(mk);
        Ok(())
    }

    /// Returns whether the given major-minor key pair is managed.
    pub fn is_managed(&self, mk: &str, k: &str) -> Result<bool, String> {
        let min_set = { self.0.read().map_err(ets)?.get(mk).map(Arc::clone) }; // Makes sure that the lock is dropped

        min_set
            .map(|x| -> Result<bool, String> { x.read().map_err(ets).map(|y| y.contains(k)) })
            .unwrap_or(Ok(false))
    }

    /// Returns whether the given major key is managed.
    pub fn is_managed_major(&self, mk: &str) -> Result<bool, String> {
        Ok(self.0.read().map_err(ets)?.contains_key(mk))
    }

    /// Returns a list of the major keys managed by the server.
    pub fn managed_majors(&self) -> Result<Vec<String>, String> {
        Ok(self.0.read().map_err(ets)?.iter().map(|(k, _)| k.clone()).collect::<Vec<String>>())
    }
}

/// Makes sure that `ManagedResources` is serializable by unwrapping the `RwLocks` and writing the
/// vectorized version of each map.
impl Serialize for ManagedResources {
    fn serialize<S: Serializer>(&self, serializer: S) -> Result<S::Ok, S::Error> {
        let map_snapshot: Vec<(String, Arc<_>)> = {
            let lock = self.0.read().map_err(S::Error::custom)?;
            lock.iter().map(|(k, v)| (k.clone(), v.clone())).collect()
        };

        let mut ser_map = Vec::new();

        for (s, m) in map_snapshot {
            let subset: Vec<String> = {
                let lock = m.read().map_err(S::Error::custom)?;
                lock.iter().map(|k| k.clone()).collect()
            };
            ser_map.push((s, subset));
        }

        ser_map.serialize(serializer)
    }
}

/// Makes sure that `ManagedResources` is deserializable by wrapping the vectorized maps in
/// `RwLocks`.
impl<'de> Deserialize<'de> for ManagedResources {
    fn deserialize<D: Deserializer<'de>>(deserializer: D) -> Result<Self, D::Error> {
        let de_map = Vec::<(String, Vec<String>)>::deserialize(deserializer)?;
        let mut map: HashMap<String, Arc<RwLock<HashSet<String>>>> = HashMap::new();

        for (s, vm) in de_map {
            map.insert(s, Arc::new(RwLock::new(HashSet::from_iter(vm))));
        }

        Ok(ManagedResources(RwLock::new(map)))
    }
}

#[cfg(test)]
mod test {

    use super::*;
    use serde_yaml;

    #[test]
    fn managed_resources_serde() {
        let rm = ManagedResources::new();
        rm.manage_minor("one", "d-one").unwrap();
        rm.manage_minor("one", "d-two").unwrap();
        rm.manage_minor("two", "d-one").unwrap();
        let ser = serde_yaml::to_string(&rm).unwrap();
        let de: ManagedResources = serde_yaml::from_str(&ser).unwrap();

        assert_eq!(rm.is_managed("one", "d-one"), de.is_managed("one", "d-one"));
        assert_eq!(rm.is_managed("one", "d-two"), de.is_managed("one", "d-two"));
        assert_eq!(rm.is_managed("two", "d-one"), de.is_managed("two", "d-one"));
    }
}
