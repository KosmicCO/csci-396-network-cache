use super::{CACHE_PATH, INSTANCE_PATH, CERTIFICATE_PATH};
use crate::ets;
use crate::utils::*;
use super::ManagedResources;
use crate::control::cache_handler::CacheMessage;

use file_tree::KeyedFileTree;
use rocket::http::Status;
use serde::de::DeserializeOwned;
use serde::ser::Serialize;
use std::collections::HashMap;
use std::fs::File;
use std::hash::Hash;
use std::mem;
use std::ops::{Drop, Not};
use std::path::PathBuf;
use std::sync::Mutex;
use std::thread::{self, JoinHandle};
use std::sync::Arc;
use std::sync::atomic::{AtomicBool, Ordering};

use native_tls::{TlsConnector, TlsStream, Certificate};
use std::io::Read;
use std::net::TcpStream;
use crate::log::*;

/// RAII structure for finishing a file write operation on `WriteNewCache`. It will send a signal
/// to `WriteNewCache` when it goes out of scope to solidify or delete the changes based on whether
/// the write was a success (done by calling `WriteState::success`).
pub struct WriteState<'a, T: Hash + Eq + Serialize + DeserializeOwned + Clone + Send + 'static>(
    bool,
    &'a WriteNewCache<T>,
    (String, String, T),
);

impl<'a, T> WriteState<'a, T>
where
    T: Hash + Eq + Serialize + DeserializeOwned + Clone + Send + 'static,
{
    /// Called when writing is finished and was a success.
    pub fn success(mut self) {
        self.0 = true;
    }
}

/// Defines the drop behavior to finish writting.
impl<'a, T> Drop for WriteState<'a, T>
where
    T: Hash + Eq + Serialize + DeserializeOwned + Clone + Send + 'static,
{
    fn drop(&mut self) {
        let _ = self.1.finish_write(&self.2, self.0);
    }
}

/// Creates a handle that stops the communication thread between the cache and the control servers
/// when `join` is called.
pub struct CacheJoinHandle {
    running: Arc<AtomicBool>,
    handle: JoinHandle<()>,
}

impl CacheJoinHandle {
    /// Stops the communication thread to the control server.
    pub fn join(self) -> thread::Result<()> {
        self.running.store(false, Ordering::Relaxed);
        self.handle.join()
    }
}

/// Manages the file cache system.
pub struct WriteNewCache<T>
where
    T: Hash + Eq + Serialize + DeserializeOwned + Clone + Send + 'static,
{
    pub managed: ManagedResources,
    existing: Mutex<HashMap<(String, String, T), bool>>,
    files: Mutex<KeyedFileTree<(String, String, T)>>,
    cur_dir: PathBuf,
}

impl<T> WriteNewCache<T>
where
    T: Hash + Eq + Serialize + DeserializeOwned + Clone + Send + 'static,
{
    /// Loads the cached-file map from `cache/file_cache.bin`. Creates a new instance if the file
    /// does not exist.
    pub fn load() -> Result<Self, String> {
        let cur_dir = PathBuf::from(env!("CARGO_MANIFEST_DIR"));

        let mut instance_path = cur_dir.clone();
        instance_path.push(INSTANCE_PATH);
        let (managed, existing, files_map) = if let Ok(file) = File::open(instance_path) {
            bincode::deserialize_from(file).map_err(ets)?
        } else {
            (ManagedResources::new(), HashMap::new(), HashMap::new())
        };

        let existing = Mutex::new(existing);
        let mut cache_path = cur_dir.clone();
        cache_path.push(CACHE_PATH);
        let files = Mutex::new(KeyedFileTree::from_existing(cache_path, files_map));

        Ok(Self {
            cur_dir,
            existing,
            files,
            managed,
        })
    }

    /// Saves the instance to `cache/file_cache.bin`.
    pub fn save(&self) -> Result<(), String> {
        // This piece of code is a nightmare. The problem is that the library object KeyedFileTree cannot be saved
        // without destroying the instance, so a second instance must first be created and used to
        // replace the first, then save and recreate it, all while keeping the locks on the
        // mutexes.

        let existing = self.existing.lock().map_err(ets)?;
        let mut files = self.files.lock().map_err(ets)?;
        let temp_files = mem::replace(&mut *files, KeyedFileTree::new(false).map_err(ets)?); // Getting ownership of the tree.
        let files_map = temp_files.get_existing_files(); // Get the inner hashmap by consuming the tree.
        let save = (&self.managed, &*existing, &files_map);

        let mut instance_path = self.cur_dir.clone();
        instance_path.push(INSTANCE_PATH);
        // Need to fail after reinstating the tree, hence no ? idioms
        let result = match File::create(instance_path).map_err(ets) {
            Ok(file) => bincode::serialize_into(file, &save).map_err(ets),
            Err(e) => Err(e),
        };

        // Now we reconstruct the KeyedFileTree and put it back
        let mut cache_path = self.cur_dir.clone();
        cache_path.push(CACHE_PATH);
        let _ = mem::replace(
            &mut *files,
            KeyedFileTree::from_existing(cache_path, files_map),
        );
        result
    }

    /// Gets the path to the file associated with the key, returning `Status::NotFound` if it has
    /// not been finished writing to yet and `Status::InternalServerError` on other errors (such as
    /// locking errors).
    pub fn get(&self, k: &(String, String, T)) -> Result<PathBuf, Status> {
        self.managed
            .is_managed(&k.0, &k.1)
            .or(Err(Status::InternalServerError))?
            .then_some(())
            .ok_or(Status::NotFound)?;

        {
            self.existing
                .lock()
                .or(Err(Status::InternalServerError))?
                .get(k)
                .ok_or(Status::NotFound)?
                .then_some(())
                .ok_or(Status::NotFound)?;
        } // Makes sure that the lock is dropped

        self.files
            .lock()
            .or(Err(Status::InternalServerError))?
            .get(k.clone())
            .or(Err(Status::InternalServerError))
    }

    /// Returns a path to a new file and a `WriteState` to manage the state of the file after
    /// reading is completed.
    pub fn start_write<'a>(
        &'a self,
        k: &(String, String, T),
    ) -> Result<(PathBuf, WriteState<'a, T>), Status> {
        self.managed
            .is_managed(&k.0, &k.1)
            .or(Err(Status::InternalServerError))?
            .then_some(())
            .ok_or(Status::NotFound)?;

        {
            let mut existing = self.existing.lock().or(Err(Status::InternalServerError))?;
            existing
                .contains_key(k)
                .not()
                .then_some(())
                .ok_or(Status::Conflict)?;
            existing.insert(k.clone(), false);
        } // Makes sure that the lock is dropped

        let path = self
            .files
            .lock()
            .or(Err(Status::InternalServerError))?
            .get(k.clone())
            .or(Err(Status::InternalServerError))?;

        Ok((path, WriteState(false, self, k.clone())))
    }

    /// Function for `WriteState` to call. Should solidify the written file on success and delete
    /// it on failure.
    fn finish_write(&self, k: &(String, String, T), success: bool) -> Result<(), String> {
        if success {
            self.existing.lock().map_err(ets)?.insert(k.clone(), true);
        } else {
            self.existing.lock().map_err(ets)?.remove(k);
        }
        Ok(())
    }

    /// Opens a TLS connection to the control server.
    fn join_control(&self, ctrl_ip: &str, ctrl_port: u16, local_ip: &str) -> Result<TlsStream<TcpStream>, String> {

        let mut c_file = File::open(CERTIFICATE_PATH).map_err(ets)?;
        let mut certificate = vec![];
        c_file.read_to_end(&mut certificate).map_err(ets)?;
        let certificate = Certificate::from_pem(&certificate).map_err(ets)?;

        let connector = TlsConnector::builder().add_root_certificate(certificate).build().map_err(ets)?;
        debug!("Starting TCP Connection.");
        let stream = TcpStream::connect((ctrl_ip, ctrl_port)).map_err(ets)?;
        debug!("Starting TLS Handshake.");
        let mut stream = connector
            .connect(ctrl_ip, stream)
            .map_err(ets)?;

        debug!("Sending ip and managed majors.");
        ser_send(&false, &mut stream)?;
        ser_send(&local_ip.to_string(), &mut stream)?;
        ser_send(&self.managed.managed_majors()?, &mut stream)?;
        return Ok(stream);
    }

    /// Parses a message gotten from the control server.
    fn take_message(&self, stream: &mut TlsStream<TcpStream>) -> Result<bool, String> {
        let message: CacheMessage = de_recv(stream)?;

        debug!("Parsing message: {:?}", message);

        match message {
            CacheMessage::Close => return Ok(true),
            CacheMessage::Manage((maj, min)) => {
                self.managed.manage_minor(&maj, &min)?;
            }
            CacheMessage::ManageMajor(maj) => {
                let not_managed = !self.managed.is_managed_major(&maj)?;
                ser_send(&not_managed, stream)?;
                if not_managed {
                    info!("Managing new major {}.", maj);
                    self.managed.manage_major(&maj)?;
                } else {
                    warn!("Already managing major {}.", maj);
                }
            }
        }
        Ok(false)
    }

    /// Starts the control communication thread and handles messages from the control server.
    pub fn start_handler(cache: Arc<Self>, ctrl_ip: &str, ctrl_port: u16, local_ip: &str) -> Result<CacheJoinHandle, String> {
        let running = Arc::new(AtomicBool::new(true));
        let thread_running = running.clone();

        let mut stream = cache.join_control(ctrl_ip, ctrl_port, local_ip)?;

        let handle = thread::spawn(move || {
            while thread_running.load(Ordering::Relaxed) {
                match cache.take_message(&mut stream) {
                    Ok(true) => {
                        cache.save().unwrap();
                        break;
                    }
                    Err(_) => break,
                    Ok(false) => (),
                }
            }
        });

        Ok(CacheJoinHandle { running, handle })
    }
}
