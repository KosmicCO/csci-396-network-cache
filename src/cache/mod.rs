pub mod write_new_disk_cache;

pub use write_new_disk_cache::*;

pub mod managed_resources_set;

pub use managed_resources_set::*;

pub const CACHE_PATH: &str = "cache/cached/";
pub const INSTANCE_PATH: &str = "cache/file_cache.bin";
pub const CERTIFICATE_PATH: &str = "crypto/cert.pem";
