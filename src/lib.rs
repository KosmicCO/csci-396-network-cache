#![feature(result_flattening)]
#![feature(bool_to_option)]

pub mod dns;
pub mod control;
pub mod cache;

pub mod utils;

/// Maps errors to string, used in conjunction with `Result::map_err`.
pub fn ets<T: ToString>(x: T) -> String {
    x.to_string()
}

pub mod log {
    pub use log::{info, warn, error, debug, trace};
}

pub const CONTROL_IP_PATH: &str = "crypto/control-ip.txt";

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }
}
