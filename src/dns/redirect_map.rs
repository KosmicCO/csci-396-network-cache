use crate::ets;

use std::collections::HashMap;
use std::sync::{Arc, RwLock};
use serde::ser::{Serializer, Serialize, Error};
use serde::de::{Deserializer, Deserialize};
use std::iter::FromIterator;

/// A map for storing minor keys against the ip of the cache that manages that resource.
pub struct ResourceMap(RwLock<HashMap<String, String>>);

impl ResourceMap {
    /// creates a new instance.
    pub fn new() -> Self {
        Self(RwLock::new(HashMap::new()))
    }

    /// Inserts a minor, ip pair
    pub fn insert(&self, k: &str, v: &str) -> Result<Option<String>, String> {
        self.0
            .write()
            .map_err(ets)
            .map(|mut x| x.insert(k.to_string(), v.to_string()))
    }

    /// Gets the ip associated with the minor key.
    pub fn get(&self, k: &str) -> Result<Option<String>, String> {
        self.0
            .write()
            .map_err(ets)
            .map(|x| x.get(k).map(String::clone))
    }

    /// Removes the ip associated with the minor key.
    pub fn remove(&self, k: &str) -> Result<Option<String>, String> {
        self.0
            .write()
            .map_err(ets)
            .map(|mut x| x.remove(k))
    }

    /// Gets a list of all the minor keys.
    pub fn get_rsrcs(&self) -> Result<Vec<String>, String> {
        Ok(self.0
            .read()
            .map_err(ets)?
            .iter()
            .map(|(k, _)| k.clone())
            .collect::<Vec<String>>())
    }
}

/// Map between major-minor keys and ip values.
pub struct MasterResourceMap(RwLock<HashMap<String, Arc<ResourceMap>>>);

impl MasterResourceMap {
    /// Creates a new instance.
    pub fn new() -> Self {
        Self(RwLock::new(HashMap::new()))
    }

    /// Inserts a major-minor key and ip pair assuming the major key has already been inserted. Will return an
    /// `Err` if the major key is not already in the map.
    pub fn insert(&self, majk: &str, k: &str, v: &str) -> Result<Option<String>, String> {
        self.0
            .read()
            .map_err(ets)
            .map(|x| x.get(majk).map(|x| x.insert(k, v)).transpose().map(Option::flatten))
            .flatten()
    }

    /// Returns whether the specific major is in the map.
    pub fn has_major(&self, majk: &str) -> Result<bool, String> {
        self.0
            .read()
            .map_err(ets)
            .map(|x| x.contains_key(majk))
    }

    /// Inserts a major-minor key and ip pair regardless of whether the major key is in the map
    /// already.
    pub fn insert_full(&self, majk: &str, k: &str, v: &str) -> Result<Option<String>, String> {
        if !self.has_major(majk)? {
            self.insert_new(majk)?;
        }
        self.insert(majk, k, v)
    }

    /// Inserts a new major key without any associated minor keys.
    pub fn insert_new(&self, majk: &str) -> Result<(), String> {
        self.0
            .write()
            .map_err(ets)?
            .insert(majk.to_string(), Arc::new(ResourceMap::new()));
        Ok(())
    }

    /// Gets the ip associated with the given major and minor keys.
    pub fn get_route(&self, majk: &str, k: &str) -> Result<Option<String>, String> {
        self.0
            .read()
            .map_err(ets)?
            .get(majk)
            .map(|x| x.get(k))
            .transpose()
            .map(Option::flatten)
    }

    /// Removes the minor key mapping from the major key. Note that this does not remove the major
    /// key from the map.
    pub fn remove(&self, majk: &str, k: &str) -> Result<Option<String>, String> {
        self.0
            .write()
            .map_err(ets)?
            .get(majk)
            .map(|x| x.remove(k))
            .transpose()
            .map(Option::flatten)
    }

    /// Gets the list of major keys stored in the map.
    pub fn get_major_rsrcs(&self) -> Result<Vec<String>, String> {
        Ok(self.0
            .read()
            .map_err(ets)?
            .iter()
            .map(|(k, _)| k.clone())
            .collect::<Vec<String>>())
    }

    /// Gets the list of minor keys associated with the given major key.
    pub fn get_minor_rsrcs(&self, majk: &str) -> Result<Option<Vec<String>>, String> {
        self.0
            .read()
            .map_err(ets)?
            .get(majk)
            .map(|x| x.get_rsrcs())
            .transpose()
    }
}

/// Makes the `MasterResourceMap` serializable by unwrapping the hashmaps from the `RwLocks` and then storing each as a list.
impl Serialize for MasterResourceMap {
    fn serialize<S: Serializer>(&self, serializer: S) -> Result<S::Ok, S::Error> {
        let map_snapshot: Vec<(String, Arc<_>)> = {
            let lock = self.0.read().map_err(S::Error::custom)?;
            lock.iter().map(|(k, v)| (k.clone(), v.clone())).collect()
        };

        let mut ser_map = Vec::new();

        for (s, m) in map_snapshot {
            let submap: Vec<(String, String)> = {
                let lock = m.0.read().map_err(S::Error::custom)?;
                lock.iter().map(|(k, v)| (k.clone(), v.clone())).collect()
            };
            ser_map.push((s, submap));
        }

        ser_map.serialize(serializer)
    }

}

/// Makes the `MasterResourceMap` deserializable by wrapping the hashmaps into `RwLocks` and `Arc` structures.
impl<'de> Deserialize<'de> for MasterResourceMap {
    fn deserialize<D: Deserializer<'de>>(deserializer: D) -> Result<Self, D::Error> {
        let de_map = Vec::<(String, Vec<(String, String)>)>::deserialize(deserializer)?;
        let mut map: HashMap::<String, Arc<ResourceMap>> = HashMap::new();

        for (s, vm) in de_map {
            map.insert(s, Arc::new(ResourceMap(RwLock::new(HashMap::from_iter(vm)))));
        }

        Ok(MasterResourceMap(RwLock::new(map)))
    }
}

#[cfg(test)]
mod test {

    use serde_yaml;
    use super::*;

    #[test]
    fn master_resource_map_serde() {
        let rm = MasterResourceMap::new();
        rm.insert_new("one").unwrap();
        rm.insert_new("two").unwrap();
        rm.insert("one", "d-one", "blah").unwrap();
        rm.insert("one", "d-two", "bleh").unwrap();
        rm.insert("two", "d-one", "bahl").unwrap();
        let ser = serde_yaml::to_string(&rm).unwrap();
        let de: MasterResourceMap = serde_yaml::from_str(&ser).unwrap();

        assert_eq!(rm.get_route("one", "d-one"), de.get_route("one", "d-one"));
        assert_eq!(rm.get_route("one", "d-two"), de.get_route("one", "d-two"));
        assert_eq!(rm.get_route("two", "d-one"), de.get_route("two", "d-one"));

    }
}




