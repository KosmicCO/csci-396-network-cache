use super::MasterResourceMap;
use crate::control::dns_handler::DNSMessage;
use crate::control::updates_list::*;
use crate::control::CERTIFICATE_PATH;
use crate::utils::*;
use crate::ets;

use bincode;
use serde::{Deserialize, Serialize};
use std::fs::File;
use std::path::PathBuf;
use std::sync::atomic::{AtomicBool, Ordering};
use std::sync::Arc;
use std::thread::{self, JoinHandle};
use std::time::{Duration, SystemTime};

use native_tls::{TlsConnector, TlsStream, Certificate};
use std::io::Read;
use std::net::TcpStream;
use crate::log::*;

const MRM_PATH: &str = "dns/map.bin";
const SAVE_SETBACK: u64 = 600;

/// Dns Update Handler Map. A struct for storing the last save time and main map.
#[derive(Serialize, Deserialize)]
struct DUHMap {
    time_last_save: Option<SystemTime>,
    mrm: Arc<MasterResourceMap>,
}

/// Stores the current directory along with the map information so that it can serialize the map
/// data.
pub struct DnsUpdateHandler {
    cur_dir: PathBuf,
    map: DUHMap,
}

/// A handle for stopping the thread that the dns server uses to communicate with the control
/// server.
pub struct DUHJoinHandle {
    running: Arc<AtomicBool>,
    handle: JoinHandle<()>,
}

impl DUHJoinHandle {
    /// Stops and joins the control communication thread.
    pub fn join(self) -> thread::Result<()> {
        self.running.store(false, Ordering::Relaxed);
        self.handle.join()
    }
}

impl DnsUpdateHandler {
    /// Loads the `DnsUpdateHandler` from the file `dns/map.bin`.
    pub fn load() -> Result<Self, String> {
        let cur_dir = PathBuf::from(env!("CARGO_MANIFEST_DIR"));

        let mut map_path = cur_dir.clone();
        map_path.push(MRM_PATH);
        let map = if let Ok(file) = File::open(map_path) {
            bincode::deserialize_from(file).map_err(ets)?
        } else {
            DUHMap {
                time_last_save: None,
                mrm: Arc::new(MasterResourceMap::new()),
            }
        };

        Ok(Self {
            cur_dir,
            map,
        })
    }

    /// Gets an `Arc` to the map stored by the handler.
    pub fn get_map(&self) -> Arc<MasterResourceMap> {
        self.map.mrm.clone()
    }

    /// Saves the map to `dns/map.bin`. Note that this function is blocking.
    pub fn save(&self) -> Result<(), String> {
        let time_last_save = SystemTime::now().checked_sub(Duration::new(SAVE_SETBACK, 0));

        let map = DUHMap {
            time_last_save,
            mrm: self.map.mrm.clone(),
        };

        let mut map_path = self.cur_dir.clone();
        map_path.push(MRM_PATH);
        bincode::serialize_into(File::create(map_path).map_err(ets)?, &map)
            .map_err(ets)?;

        Ok(())
    }

    /// Initializes the communication thread between the dns server and the control server.
    fn join_control(&self, ctrl_ip: &str, ctrl_port: u16) -> Result<TlsStream<TcpStream>, String> {

        let mut c_file = File::open(CERTIFICATE_PATH).map_err(ets)?;
        let mut certificate = vec![];
        c_file.read_to_end(&mut certificate).map_err(ets)?;
        let certificate = Certificate::from_pem(&certificate).map_err(ets)?;

        let connector = TlsConnector::builder().add_root_certificate(certificate).build().map_err(ets)?;
        debug!("Starting TCP Connection.");
        let stream = TcpStream::connect((ctrl_ip, ctrl_port)).map_err(ets)?;
        debug!("Starting TLS Handshake.");
        let mut stream = connector
            .connect(ctrl_ip, stream)
            .map_err(ets)?;

        let bytes = bincode::serialize::<Option<SystemTime>>(&self.map.time_last_save).map_err(ets)?; // Outputs the serialized version of the system time for debug purposes.
        debug!("{:?}", bytes);

        debug!("Sending prev updated time: {:?}.", self.map.time_last_save);
        ser_send(&true, &mut stream)?;
        ser_send(&self.map.time_last_save, &mut stream)?;
        return Ok(stream);
    }

    /// Mutates the resource map depending on the resource updates received from the control
    /// server.
    fn take_update(&self, rsrc: (String, String), update: UpdateOp) -> Result<(), String> {
        match update {
            UpdateOp::Delete => {
                info!("Resource {:?} deleted.", rsrc);
                self.map.mrm.remove(&rsrc.0, &rsrc.1)?;
            }
            UpdateOp::Move(ip) => {
                info!("Resource {:?} moved to {}.", rsrc, ip);

                self.map.mrm.insert_full(&rsrc.0, &rsrc.1, &ip)?;
            }
        }
        Ok(())
    }

    /// Blocks and waits for the next message sent by the control server.
    fn take_message(&self, stream: &mut TlsStream<TcpStream>) -> Result<bool, String> {
        let message: DNSMessage = de_recv(stream)?;

        debug!("Parsing message: {:?}", message);

        match message {
            DNSMessage::Close => return Ok(true),
            DNSMessage::Update(rsrc, update) => {
                self.take_update(rsrc, update)?;
            }
        }
        Ok(false)
    }

    /// Starts the control communication thread and returns a handler for that thread.
    pub fn start_handler(duh: Arc<Self>, ctrl_ip: &str, ctrl_port: u16) -> Result<DUHJoinHandle, String> {
        let running = Arc::new(AtomicBool::new(true));
        let thread_running = running.clone();

        let mut stream = duh.join_control(ctrl_ip, ctrl_port)?;

        let handle = thread::spawn(move || {
            while thread_running.load(Ordering::Relaxed) {
                match duh.take_message(&mut stream) {
                    Ok(true) => {
                        duh.save().unwrap();
                        break;
                    }
                    Err(_) => break,
                    Ok(false) => (),
                }
            }
        });

        Ok(DUHJoinHandle { running, handle })
    }
}
