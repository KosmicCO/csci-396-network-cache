use net_cache::control::tasker::{Tasker, Task};
use net_cache::utils::*;
use net_cache::log::*;

use std::sync::Arc;
use std::thread;
use std::io::prelude::*;

use shrust::{Shell, ShellIO};

fn main() {

    setup_logger("control/logging.log").unwrap();

    let (_, port) = get_control_ip();
    let mut ip = "0.0.0.0:".to_string();
    ip.push_str(&port.to_string());

    let tasker = Arc::new(Tasker::new());

    info!("Starting control server.");

    let handle = {
        let tasker = tasker.clone();
        thread::spawn(move || {
            info!("Starting control loop in thread.");
            Tasker::init_control(tasker, &ip)
            //TODO: Take ip from file
        })
    };
    let mut shell = Shell::new(tasker.clone());

    shell.new_command("newrsrc", 
        "Adds a new resource type to the cache.",
        2,
        |io, tasker, s| {
            if let [maj, min] = s {
                let rsrc = ((*maj).to_string(), (*min).to_string());
                writeln!(io, "Adding resource {:?}.", rsrc)?;
                if tasker.push_task(Task::NewResource(rsrc)).is_err() {
                    writeln!(io, "Error pushing task, control probably closed.")?;
                }
            } else {
                writeln!(io, "Needs 2 arguments.")?;
            }
            Ok(())
        });

    shell.new_command_noargs("caches",
        "Prints out the ips of the registered cache servers.",
        |io, tasker| {
            if tasker.push_task(Task::PrintRegistered).is_err() {
                writeln!(io, "Error pushing task, control probably closed.")?;
            }
            Ok(())
        });

    shell.new_command("managemajor",
        "Tells the cache server with the given ip to manage the resources with the given major.",
        2,
        |io, tasker, s|{
            if let [maj, ip] = s {
                writeln!(io, "Having cache at {} handle {}.", ip, maj)?;
                if tasker.push_task(Task::ManageMajor(maj.to_string(), ip.to_string())).is_err() {
                    writeln!(io, "Error pushing task, control probably closed.")?;
                }
            } else {
                writeln!(io, "Need 2 arguments.")?;
            }
            Ok(())
        });

    shell.run_loop(&mut ShellIO::default());

    info!("Closing control.");

    if tasker.push_task(Task::Stop).is_ok() {
        handle.join().unwrap().unwrap();
    }
}
