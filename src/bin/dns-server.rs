#![feature(proc_macro_hygiene, decl_macro)]

//#[allow(dead_code)]

#[macro_use]
extern crate rocket;

use net_cache::dns::*;
use net_cache::utils::*;
use net_cache::ets;
use net_cache::log::*;

use rocket::http::{RawStr, Status};
use rocket::response::Redirect;
use rocket::State;
use std::path::PathBuf;
use std::sync::Arc;
use std::thread::{self, JoinHandle};

use shrust::{Shell, ShellIO};
use std::io::prelude::*;

/// Gets the route and returns an appropriate response.
fn reroute(
    map: &MasterResourceMap,
    rsrc_type: &str,
    rscr_header: &str,
    resource: PathBuf,
) -> Result<Redirect, Status> {
    let res = map.get_route(rsrc_type, rscr_header);
    match res {
        Err(e) => {
            error!("Unable to get route:\n{}", e);
            Err(Status::InternalServerError)
        },
        Ok(None) => Err(Status::NotFound),
        Ok(Some(url)) => Ok(Redirect::permanent(format!(
            "{}/{}/{}/{}",
            url,
            rsrc_type,
            rscr_header,
            resource.to_str().ok_or(Status::BadRequest)?
        ))),
    }
}

/// Runs on a Get request.
#[get("/<r_type>/<r_head>/<resource..>")]
fn rd_get(
    map: State<Arc<MasterResourceMap>>,
    r_type: &RawStr,
    r_head: &RawStr,
    resource: PathBuf,
) -> Result<Redirect, Status> {
    info!("Serving new request.");
    reroute(map.inner(), r_type, r_head, resource)
}

/// Runs on a POST request.
#[post("/<r_type>/<r_head>/<resource..>")]
fn rd_post(
    map: State<Arc<MasterResourceMap>>,
    r_type: &RawStr,
    r_head: &RawStr,
    resource: PathBuf,
) -> Result<Redirect, Status> {
    info!("Serving new request.");
    reroute(map.inner(), r_type, r_head, resource)
}

/// Starts the shell interface.
fn start_shell(dup: Arc<DnsUpdateHandler>) -> JoinHandle<()> {
    let mut shell = Shell::new(dup);

    shell.new_command(
        "poll",
        "Gives the redirect address associated with the given resource if it exists.",
        2,
        |io, dup, s| {
            if let [maj, min] = s {
                let rsrc = (*maj, *min);
                if let Ok(route) = dup.get_map().get_route(rsrc.0, rsrc.1) {
                    if let Some(route) = route {
                        writeln!(io, "Route for {:?}: {}", rsrc, route)?;
                    } else {
                        writeln!(io, "Route doesn't exist for {:?}", rsrc)?;
                    }
                }
            } else {
                writeln!(io, "Needs 2 arguments.")?;
            }
            Ok(())
        },
    );

    shell.new_command(
        "majors",
        "Gives the major resources currently stored.",
        0,
        |io, dup, _| {
            if let Ok(rsrcs) = dup.get_map().get_major_rsrcs() {
                writeln!(io, "{}", rsrcs.join("\n"))?;
            }
            Ok(())
        },
    );

    shell.new_command(
        "minors",
        "Gives the minor resources currently associated with the given major resource.",
        1,
        |io, dup, s| {
            if let [maj] = s {
                if let Ok(rsrcs) = dup.get_map().get_minor_rsrcs(*maj) {
                    if let Some(rsrcs) = rsrcs {
                        writeln!(io, "{}", rsrcs.join("\n"))?;
                    } else {
                        writeln!(io, "Major resource doesn't exist: {}", *maj)?;
                    }
                }
            }
            Ok(())
        },
    );

    shell.new_command_noargs(
        "save",
        "Saves the current mappings of the resources. Note that this process is expensive and blocking.",
        |io, dup| {
            match dup.save() {
                Ok(()) => writeln!(io, "Save successful.")?,
                Err(e) => writeln!(io, "{}", e)?,
            }
            Ok(())
        });

    thread::spawn(move || shell.run_loop(&mut ShellIO::default()))
}

fn main() -> Result<(), String> {
    setup_logger("dns/logging.log").map_err(ets)?;
    
    let (ip, port) = get_control_ip();

    let dns_handler = Arc::new(DnsUpdateHandler::load()?);
    let mrm = dns_handler.get_map();
    let dns_join = DnsUpdateHandler::start_handler(dns_handler.clone(), &ip, port)?;
    let shell_join = start_shell(dns_handler);
    rocket::ignite()
        .manage(mrm)
        .mount("/", routes![rd_get, rd_post])
        .launch();
    dns_join
        .join()
        .map_err(|_| "Error joining dns handler thread.")?;
    shell_join
        .join()
        .expect("Error joining shell handler thread.");
    Ok(())
}
