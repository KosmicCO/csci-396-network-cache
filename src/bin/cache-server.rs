#![feature(proc_macro_hygiene, decl_macro)]

#[macro_use]
extern crate rocket;

use net_cache::cache::{CacheJoinHandle, WriteNewCache};
use net_cache::ets;
use net_cache::utils::*;

use rocket::data::Data;
use rocket::http::{RawStr, Status};
use rocket::response::Stream;
use rocket::State;
use std::fs::File;
use std::path::PathBuf;
use std::sync::Arc;

use shrust::{Shell, ShellIO};
use std::io::prelude::*;
use std::thread::{self, JoinHandle};

type Cache = Arc<WriteNewCache<PathBuf>>;

const CHUNK_SIZE: u64 = 2048;
const CACHE_IP_PATH: &str = "cache/cache-ip.txt";

/// Runs on a GET request. Returns a stream of the file data on success.
#[get("/<r_type>/<r_head>/<resource..>")]
fn cache_get(
    cache: State<Cache>,
    r_type: &RawStr,
    r_head: &RawStr,
    resource: PathBuf,
) -> Result<Stream<File>, Status> {
    let key = (r_type.to_string(), r_head.to_string(), resource.clone());
    let file_path = cache.get(&key)?;
    let file = File::open(file_path).or(Err(Status::InternalServerError))?;
    Ok(Stream::chunked(file, CHUNK_SIZE))
}

/// Runs on a POST request. Streams incoming data to a file given by the system on success.
#[post("/<r_type>/<r_head>/<resource..>", data = "<data>")]
fn cache_post(
    cache: State<Cache>,
    r_type: &RawStr,
    r_head: &RawStr,
    resource: PathBuf,
    data: Data,
) -> Status {
    let key = (r_type.to_string(), r_head.to_string(), resource.clone());
    match cache.start_write(&key) {
        Ok((file_path, stat)) => {
            if let Err(_) = data.stream_to_file(file_path) {
                return Status::InternalServerError;
            }
            stat.success();
            return Status::Ok;
        }
        Err(status) => return status,
    }
}

/// Starts the shell interface.
fn start_shell(cache: Cache, cache_handle: Option<CacheJoinHandle>) -> JoinHandle<()> {
    let mut shell = Shell::new((cache, cache_handle));

    shell.new_command(
        "poll",
        "Gives the first 3 lines of the resource if it exists.",
        3,
        |io, (cache, _), s| {
            if let [maj, min, path] = s {
                let mut r_path = PathBuf::new();
                r_path.push(*path);
                let key = (maj.to_string(), min.to_string(), r_path);
                if let Ok(file_path) = cache.get(&key) {
                    if let Ok(lines) = read_lines(file_path) {
                        for (i, line) in lines.enumerate() {
                            if i == 3 {
                                break;
                            }
                            if let Ok(line) = line {
                                writeln!(io, "{}", line)?;
                            } else {
                                writeln!(io, "{}", "Failed to write line.")?;
                                break;
                            }
                        }
                    } else {
                        writeln!(io, "Failed to open resource.")?;
                    }
                } else {
                    writeln!(io, "Resource does not exist or failed to poll.")?;
                }
            } else {
                writeln!(io, "Needs 3 arguments.")?;
            }
            Ok(())
        },
    );

    shell.new_command_noargs(
        "save",
        "Saves the cached contents. Note that this operation is very slow and blocks.",
        |io, (cache, _)| {
            match cache.save() {
                Ok(()) => writeln!(io, "Save successful.")?,
                Err(e) => writeln!(io, "{}", e)?,
            }
            Ok(())
        },
    );

    shell.new_command_noargs(
        "stop",
        "Stops taking input from control so that the server can safely be saved and shutdown.",
        |io, (cache, handle)| {
            if let Some(handle) = std::mem::take(handle) {
                if let Err(_) = handle.join() {
                    writeln!(io, "Error when closing connection to control.")?;
                } else {
                    writeln!(io, "Successfully stopped connection to control.")?;
                }
            } else {
                writeln!(io, "Connection to control already closed.")?;
            }
            if let Err(_) = cache.save() {
                writeln!(io, "Failed to save cache.")?;
            } else {
                writeln!(io, "Saved cache successfully")?;
            }
            Ok(())
        },
    );

    thread::spawn(move || shell.run_loop(&mut ShellIO::default()))
}

/// Gets the reroute ip for this cache server from `cache/cache-ip.txt`.
pub fn get_cache_ip() -> String {

    let mut ip_file = PathBuf::from(env!("CARGO_MANIFEST_DIR"));
    ip_file.push(CACHE_IP_PATH);
    let mut cache_ip_lines =
        read_lines(ip_file).expect("Failed to read file crypto/control-ip.txt");
    let ip = cache_ip_lines
        .next()
        .expect("Failed to get the ip of the control server from crypto/control-ip.txt.")
        .expect("Failed to get the ip of the control server from crypto/control-ip.txt.");

    ip
}

fn main() -> Result<(), String> {
    setup_logger("cache/logging.log").map_err(ets)?;

    let (ip, port) = get_control_ip();
    let cache_ip = get_cache_ip();

    let cache: Cache = Arc::new(WriteNewCache::load()?);
    let shell_join = start_shell(
        cache.clone(),
        Some(WriteNewCache::start_handler(cache.clone(), &ip, port, &cache_ip)?),
    );
    rocket::ignite()
        .manage(cache)
        .mount("/", routes![cache_get, cache_post])
        .launch();
    shell_join
        .join()
        .expect("Error joining shell handler thread.");
    Ok(())
}
