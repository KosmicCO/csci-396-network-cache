# Network Cache

By Cruz Barnum

## Documentation

The project can be found on [GitLab](https://gitlab.com/KosmicCO/csci-396-network-cache).
Docs can also be found at `doc/net_cache/index.html`.

## Project Description

The project implements a distributed k-v store using a multi-server dns and cache network which can have multiple dns servers that clients use to redirect to the cache server that has the desired resource.
It is intended as a caching system to work in conjunction with a web app that must store data on a server.
Each cache server can be tuned to only accept certain kinds of resources using a major-minor tagging system, where a major tag is the type of resource and the minor tag is the specific category it is from (maybe accounts or ect.). 
As example of a resource location would be
`read-only/myuser/homework.txt`,
where `read-only` might be a major tag for read-only data, `myuser` would be a minor tag for user profiles using read only data, and `homework.txt` would be the specific target resource.

Each cache server supports single write, always read, where a resource can only be written to once, but can be read from any number of times.
This was due to specific library and time constraints, which will be discussed later.

Note that the dns servers are single-layer, but there can be many of them and each forms a connection with the control server, which keeps them updated (maybe that makes them multi-layer).

## Startup

To startup each server, get a copy of this repo onto the desired servers and follow the startup instructions for each type of server (control server, cache server, dns server) located in their respective directories.

To start testing, go to the control-server panel and have a cache server manage a major tag (such as `user-data`) so that it can be assigned new users as they come in.
Also note that multiple cache servers can be tasked with managing the same major tag; the minor tags will be distributed among them as required.
The above can be done in the following way:

1. See what cache servers are connected to the control server by typing `caches` into the server.
2. For each server that you wish to assign a major tag, type the command `managemajor <major> <cache-ip>`.

Next, minor tags can be assigned.
For this example, we will have minor tags be users so that they can store files on our server.
For each user, type the command
`newrsrc <major> <minor>`
where it might look like
`newrsrc user-data edward`.
The different user data will be distributed evenly across the different cache servers managing the `user-data` tag.

Next, we can then start to use the cache using GET and POST requests.
If either a GET or POST request is sent to the DNS server, it will respond with a redirect response that gives us the hostname of the appropriate cache server to send our request to.
We then send our request to the given cache server.
Note that the cache server will respond with 404 if the major and/or minor resources are not managed by the server and will repond with 409 if we are trying to POST to a file that already exists.
Note that the server streams large files to and from the client so that the transaction goes faster and does not use a lot of server resources.

Following is an example of a telnet transaction with the system:
We first poll the dns server with the intended operation.
```
$ telnet home.kosdt.us 50301
Trying 50.53.66.139...
Connected to home.kosdt.us.
Escape character is '^]'.
POST /user-data/edward/secrets/letter HTTP/1.1
Content-Length: 1

g
HTTP/1.1 308 Permanent Redirect
Location: home.kosdt.us:8000/user-data/edward/secrets/letter
Server: Rocket
Content-Length: 0
Date: Sun, 09 May 2021 00:40:50 GMT

Connection closed by foreign host.
```

We then redirect our request based on the information given by the dns server (this is automatic in some web browsers).
```
$ telnet home.kosdt.us 8000
Trying 50.53.66.139...
Connected to home.kosdt.us.
Escape character is '^]'.
POST /user-data/edward/secrets/letter HTTP/1.1
Content-Length: 1

g
HTTP/1.1 200 OK
Server: Rocket
Content-Length: 0
Date: Sun, 09 May 2021 00:44:03 GMT

Connection closed by foreign host.
```

We can then stream the data from the cache server.
```
$ telnet home.kosdt.us 8000
Trying 50.53.66.139...
Connected to home.kosdt.us.
Escape character is '^]'.
GET /user-data/edward/secrets/letter HTTP/1.1

HTTP/1.1 200 OK
Server: Rocket
Date: Sun, 09 May 2021 00:47:06 GMT
Transfer-Encoding: chunked

1
g
0

Connection closed by foreign host.
```
Note that the numbers given are an artifact of the streaming process.
A web client or browser would automatically handle the streaming for us.

After finishing, the states of the cache and dns servers can be stored by running the `save` command, or by running the `quit` command, then being exited by `ctrl+C` as required by the Rocket web-server library (discussed later).
The control server can be exited by simply typing `quit`.
The next time each is loaded, they will be able to restore the database information from their respective saves.

## Design Choices

### Rocket HTTP Library

The Rocket HTTP library makes it very convenient to handle HTTP requests and also offers multiple resources for premade response type, such as Redirecting and Streaming which are used in the project.
The Rocket library has some interesting, though small, design flaws that effect the project however.
The largest one is the fact that Rocket doesn't support graceful shutdown; namely, the program can only be exited by an interrupt, which is why `save` must be typed before exiting.
Features such as graceful shutdown are being added in future versions of Rocket though.

### File-Tree Library Cache

The cache server uses a file-tree library structure called `KeyedFileTree` to store user files.
The initial intent was to imeplement/find a library to handle loading popular files into memory and keeping them there for fast and efficient access; however, time constraints and the lack of a specific mem/file cache library meant that the File-Tree system was the best choice.
The File-Tree system simply indexes files by a key.
The files are never loaded into memory (at least not more than the operating system already does).
This was not the most limiting part though.
Because the File-Tree system is non-asynchronous, every incoming request must lock a single instance of it, which is slow and unfortunate; however, because I felt that the specifics of caching on each server was outside of the scope of the project and since it is a distributed cache system, I felt that it was fine for now.
Obvious improvements would be to implement a proper memory/file caching system that is asynchronous to maximize the usage out of any given cache server.

### TLS for Inter-Server Communication

TLS is used for communication between the various servers because it is more secure than TCP and lighter-weight than HTTP(S).
The security of the servers allows the cache and dns servers to know that they are talking to a legitimate control server; however, the reverse was not true in this implementation.
A point of improvement would be to have the reverse be true, perhaps by passing a random nonce through the TLS connection and then listening for a signed response back; though, I felt that this was outside of the scope of the project and that the TLS connection was a nice proof of concept.

## Future Extentions

Some possible extensions could be the following:
- Over-network resource redistribution.
- Autobalancing resource policies (using the above technique).
- Create a app that utilizes the cache network.
- Create some way of automatically creating minor resources (such as by creating an account).
